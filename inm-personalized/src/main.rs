#![no_std]
#![no_main]

mod panicking;

use chain_trans::prelude::Trans as _;
use embedded_hal as ehal;
use tufty2040::hal::{self, fugit::ExtU32, Clock, Watchdog};
use tufty2040::pac;

use ehal::{
    digital::v2::ToggleableOutputPin,
    watchdog::{Watchdog as _, WatchdogEnable},
};

#[tufty2040_higher::entry]
fn main() -> ! {
    let mut peripherals = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(peripherals.WATCHDOG);
    let clocks = hal::clocks::init_clocks_and_plls(
        tufty2040::XOSC_CRYSTAL_FREQ,
        peripherals.XOSC,
        peripherals.CLOCKS,
        peripherals.PLL_SYS,
        peripherals.PLL_USB,
        &mut peripherals.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();
    watchdog.start(100.millis());

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    // Single-Cycle IO pins (includes gpio but also other stuff).
    let sio = hal::sio::Sio::new(peripherals.SIO);
    let gpios = tufty2040::Pins::new(
        peripherals.IO_BANK0,
        peripherals.PADS_BANK0,
        sio.gpio_bank0,
        &mut peripherals.RESETS,
    );
    let mut user_led = gpios
        .user_led
        .into_push_pull_output_in_state(hal::gpio::PinState::High);

    let mut lcd_backlight = gpios
        .lcd_backlight
        .into_push_pull_output_in_state(hal::gpio::PinState::High);
    loop {
        user_led.toggle().unwrap_or(());
        // lcd_backlight.toggle().unwrap_or(());
        for _ in 0..50 {
            watchdog.feed();
            delay.delay_ms(400 / 50);
        }
    }
}
