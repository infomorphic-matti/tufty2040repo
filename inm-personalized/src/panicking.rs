//! Module containing a simple panic handler.

use core::panic::PanicInfo;

#[panic_handler]
fn panic(pi: &PanicInfo) -> ! {
    loop {}
}
