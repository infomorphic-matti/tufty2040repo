# tufty2040repo

One of my friends was kind enough to give me a [tufty2040](https://shop.pimoroni.com/products/tufty-2040?variant=40036912595027). However, the main languages in use are C++ or MicroPython.

As I much prefer Rust to either of those languages, this repository contains both the libraries I will write to replace their SDK with a Rust version, as well as whatever random personal projects I want to write as well. 

## Notes on Flashing
Much of the information on flashing the board with a new program is based around the fact that the bootloader mode emulates a usb flash storage device onto which you can put a `.uf2` file that then gets flashed. This does seem to work on my machine based on `lsusb`, but I can't find an actual mass storage block device anywhere (or the UART usb terminal). Instead, I use the [```picotool```](https://github.com/raspberrypi/picotool) program to perform actions on the device, which works more reliably. 

## Notes on Implementation
The bare impls are based on the [rp2040-hal](https://crates.io/crates/rp2040-hal) crate. Note that there is a similar, defunct crate called `rp-hal`, which should NOT be used.

Much implementation in these crates is based on the code in the following repositories, as they already contain the necessary information and just reading the code will provide enough for rust reimplementations:
* <https://github.com/raspberrypi/pico-sdk> - Pico SDK
* <https://github.com/pimoroni/pimoroni-pico> - Pimonori-specific stuff. This is very useful when re-implementing information relating to the pin layouts of a specific board.
* Datasheet for the RP2040 chip used by tufty2040 [^rp2040-datasheet]
* <https://github.com/raspberrypi/pico-bootrom> - Bootrom that is embedded directly in the microprocessor. This means we don't need to worry about bricking the device, as we can always jump into BOOTSEL mode with a button press on boot. This bootrom is open source, too :)
* <https://docs.rs/pimoroni_badger2040/latest/pimoroni_badger2040/> and other similar pimoroni crates. These build on top of the rp2040-hal and [rp2040_boot2](https://docs.rs/rp2040-boot2/0.3.0/rp2040_boot2/) which is needed for the chip bootloader to actually accept the code. The git repository for these useful board support crates is [here](https://github.com/rp-rs/rp-hal-boards/tree/main) 
* <https://cdn.shopify.com/s/files/1/0174/1800/files/tufty_schematic.pdf?v=1655385675> - Tufty schematic
    Useful info: The flash chip is W25Q64JVXGIQ TR (8MB), datasheet <https://www.alldatasheet.com/datasheet-pdf/pdf/1243801/WINBOND/W25Q64JVXGIQ.html> - datasheet is stored in the git repo 
* Most larger scale implementation is being built with the Embassy HALs now, because the async task model is much more comprehensible.
    * However, embassy has slightly less strongly typed internal components (though still 
      extremely strong), which is a bit annoying.


## Magic Building
Various configs and magic builder programs are needed for this to work.

Important configs for:
* the target
* the linker flags
* LTO
are included in `.cargo/config.toml` in this project.

Several programs should be installed for this:
* `elf2uf2-rs` - is on the Arch Package Repos (for automatic flashing and running of the program, not actually necessary when picotool is present)
    * Note: this doesn't work if for some reason your machine doesn't turn the BOOTSEL mode of the device into a disk-like thing like mine
    * Instead, you should install `picotool`. This workspace is actually configured to use picotool as the flashing mechanism
    * You should also use the picotool [udev rules file](https://github.com/raspberrypi/picotool/blob/master/udev/99-picotool.rules). However, I had to finangle around with it to make it work.
    * In particular:
        * I modified the udev rule file to use `SUBSYSTEMS==` rather than `SUBSYSTEM==`. I'm not 100% sure this is necessary
        * I had to make a system group with `sudo groupadd --system plugdev`
        * I had to add myself to the group with `sudo usermod -aG plugdev <username>`
        * I restarted the systemd daemons with `sudo systemctl daemon-reload`. I'm still not sure if this is necessary, but my understanding is that systemd does udev stuff.
        * Group changes don't apply until you re-login. To avoid restarting my main session, I simply did `sudo -u <username> -i` to get a subshell that actually counts as a "login"
        * I re-plugged-in the device in BOOTSEL mode. 
* `flip-link` - is on the Arch Package Repos (provides stack overflow protection on embedded systems - see [here](https://github.com/knurling-rs/flip-link))
* `picotool` - is also on the AUR as `picotool-git`. Has a udev rules file mentioned above that should be installed manually to avoid needing to use sudo. By adding yourself to the `plugdev` group, you can then use `cargo run` to automatically flash and execute built programs! 

A good bit of very well explained info on linker scripts: <https://allthingsembedded.com/post/2020-04-11-mastering-the-gnu-linker-script/>. Some more <https://wiki.osdev.org/Linker_Scripts>

## PIO
The RP2040 has an excellent feature called PIO. These are small secondary cores that run separate to the main processor that allow the precise control of GPIO pins (and in particular, this avoids lots of CPU usage for bitbanging too). 

Some resources on PIO:
* <https://circuitcellar.com/resources/quickbits/rp2040-programmable-io/>
* <https://hackspace.raspberrypi.com/articles/what-is-programmable-i-o-on-raspberry-pi-pico>

They can also be triggered by interrupt requests in certain ways, which could allow for integration with the DMA peripheral coprocessors. 

## DMA
The RP2040 (as well as some other Cortex-M0+ chips) have a collection of memory access co-processors that can make memory copying, modifying, and GPIO control (via direct register access, R or W) very efficient by performing it on secondary processors. 

Not only this, but they can do various forms of mathematical processing (e.g. AND or XOR or NOT) in a restricted manner, e.g. to select only a subset of pins. Not only that, but they have the ability to copy on a trigger interrupt as well, which could be triggered by other peripherals including PIO or other DMA.

`embedded-rp` does have DMA in their hardware abstraction layer, but they have a very restricted version of it mostly limited to copying to and from locations (with the optional ability to be triggered). It might be that we need to make our own abstractions (using the `embedded-rp` crate for inspiration on how to join it with async to match the runtime). 

It's very difficult to find detailed resources on how to implement DMA stuff. The best I've found is:
* <https://github.com/raspberrypi/pico-examples/tree/master/dma> - the pico examples directory.
* <https://blog.adafruit.com/2023/01/24/using-direct-memory-access-dma-on-the-rp2040-chip-raspberrypi-rp2040-dma/> someone implemented turing complete systems purely in DMA
* The RP2040 datasheet[^rp2040-datasheet]


[^rp2040-datasheet]: Can be found [here](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf) or inside this repository
