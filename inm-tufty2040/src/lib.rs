#![no_std]

pub use rp2040_hal as hal;

#[cfg(feature = "rt")]
pub use rp2040_hal::entry;

/// Boot loader symbol. Requires linker script cooperation
///
/// We have to use the generic one here because the flash chip on the tufty is not one where a
/// specific bootloader has been made yet.
#[cfg(feature = "boot2")]
#[link_section = ".boot2"]
#[no_mangle]
#[used]
pub static BOOTLOADER: [u8; 256] = rp2040_boot2::BOOT_LOADER_GENERIC_03H;

pub use hal::pac;

// See the schematic at [this
// location](https://cdn.shopify.com/s/files/1/0174/1800/files/tufty_schematic.pdf?v=1655385675)
// or in the main repo's datasheets folder.
hal::bsp_pins! {
    /// GPIO 0 is connected to UART tx
    Gpio0 { name: uart_tx },
    /// GPIO 1 is connected to UART rx
    Gpio1 { name: uart_rx },
    /// GPIO 2 is connected to the LCD backlight controller (maybe PWD controlled?)
    Gpio2 { name: lcd_backlight },
    /// GPIO 3 is connected to I2C_INT
    Gpio3 { name: i2c_int },
    /// GPIO 4 is connected to I2C_SDA
    Gpio4 { name: i2c_sda },
    /// GPIO 5 is connected to I2C_SCL
    Gpio5 { name: i2c_scl },
    /// GPIO 6 is connected to the "down" button, which is active high
    Gpio6 { name: button_down },
    /// GPIO 7 is connected to the "A" button, which is active high
    Gpio7 { name: button_a },
    /// GPIO 8 is connected to the "B" button, which is active high
    Gpio8 { name: button_b },
    /// GPIO 9 is connected to the "C" button, which is active high
    Gpio9 { name: button_c },
    /// GPIO 10 is connected to LCD_CS
    Gpio10 { name: lcd_cs },
    /// GPIO 11 is connected to LCD_RS
    Gpio11 { name: lcd_rs },
    /// GPIO 12 is connected to LCD_WR
    Gpio12 { name: lcd_wr },
    /// GPIO 13 is connected to LCD_RD
    Gpio13 { name: lcd_rd },
    /// GPIO 14 is connected to LCD_DB0
    Gpio14 { name: lcd_db0 },
    /// GPIO 15 is connected to LCD_DB1
    Gpio15 { name: lcd_db1 },
    /// GPIO 16 is connected to LCD_DB2
    Gpio16 { name: lcd_db2 },
    /// GPIO 17 is connected to LCD_DB3
    Gpio17 { name: lcd_db3 },
    /// GPIO 18 is connected to LCD_DB4
    Gpio18 { name: lcd_db4 },
    /// GPIO 19 is connected to LCD_DB5
    Gpio19 { name: lcd_db5 },
    /// GPIO 20 is connected to LCD_DB6
    Gpio20 { name: lcd_db6 },
    /// GPIO 21 is connected to LCD_DB7
    Gpio21 { name: lcd_db7 },
    /// GPIO 22 is connected to the "up" button, which is active high
    Gpio22 { name: button_up },
    /// GPIO 23 is connected to the "boot"/"user sw" button, which is active low
    Gpio23 { name: button_user },
    /// GPIO 24 is connected to VBUS_DETECT. This is the connection to the USB bus cable
    /// with a ground - 10㏀ - VBUS_DETECT - 5㏀ - USB bus mini-circuit.
    ///
    /// If it's high, that indicates the presence of a USB connection. if not, then we're
    /// probably getting power from battery.
    Gpio24 { name: vbus_detect_usb },
    /// GPIO 25 is connected to the user LED/activity indicator. This LED is turned on
    /// when the pin is high
    Gpio25 { name: user_led },
    /// GPIO 26 is connected to the light sensor, with an ADC (ADC0)
    /// see the sensor_power pin for more info on the voltages this can take
    Gpio26 { name: light_sensor },
    /// GPIO 27 is connected to the sensor power pin, with an ADC (ADC1)
    ///
    /// The sensor power pin can be used as an output, and it's connected to the
    /// various sensors.
    ///
    /// ## Phototransistor sensor
    /// The other side of the phototransistor is connected
    /// to the light_sensor pin, which is also connected to ground with a 1㏀ resistor.
    ///
    ///                     |||| Light (as gate)
    /// SENSOR_POWER -> phototransistor ---------> LIGHT_SENSOR -> 1k resistor -> GND
    ///
    /// The phototransistor model is PT19-21C/L41/TR8, datasheet at:
    /// * <https://www.digikey.com/htmldatasheets/production/854340/0/0/1/pt19-21c-l41-tr8.pdf>
    /// * Stored in the repository
    ///
    /// ## Battery Sensor
    /// Sensor power is also used to power the battery reference voltage generator.
    Gpio27 { name: sensor_power },
    /// GPIO 28 is connected to a reference voltage (of 1.24V), intended for battery monitoring
    /// It has an ADC (ADC2).
    Gpio28 { name: voltage_reference_1v24 },
    /// GPIO 29 is connected to the battery voltage sensor circuit with an ADC (ADC3).
    ///
    /// The battery voltage is split into 1/3 before sensing using a 750k/1.5M resistor pair.
    /// For example, the maximum voltage sensed for a 5V battery would be 1.66V (5V/3).
    Gpio29 { name: battery_sensor },
}

// The LCD connectors are for the ST7789V board.
// The datasheet can be found:
// * [on this
// website](https://www.alldatasheet.com/datasheet-pdf/pdf/1132511/SITRONIX/ST7789V.html)
// * Downloaded into the repository

/// XOSC crystal frequency.
///
/// I actually don't know this for sure but every other board crate uses this value...
pub const XOSC_CRYSTAL_FREQ: u32 = 12_000_000;

// Important information
//
// A lot of the structure of this module is related to the various other board support crates
// located at https://github.com/rp-rs/rp-hal-boards/tree/main
