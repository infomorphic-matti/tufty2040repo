/* Copied from https://github.com/rp-rs/rp-hal/blob/main/memory.x and adjusted for flash size 
* Note that there is an EXCELLENT intro to linker scripts *here*: https://allthingsembedded.com/post/2020-04-11-mastering-the-gnu-linker-script/
* Furthermore, this script is specific to crates that depend on `cortex-m-rt`. The magic names of memory and section regions
* come from the `link.x` script inside the `cortex-m-rt` crate, see:
* https://github.com/rust-embedded/cortex-m/blob/master/cortex-m-rt/link.x.in
*/
MEMORY {
    BOOT2 : ORIGIN = 0x10000000, LENGTH = 0x100
    /* Primary Program Flash - actual flash is 8192K - 0x100, but we want some for storage */
    FLASH : ORIGIN = 0x10000100, LENGTH = 3072K - 0x100
    

    /*
     * RAM consists of 4 banks, SRAM0-SRAM3, with a striped mapping.
     * This is usually good for performance, as it distributes load on
     * those banks evenly.
     */
    RAM : ORIGIN = 0x20000000, LENGTH = 256K
    /*
     * RAM banks 4 and 5 use a direct mapping. They can be used to have
     * memory areas dedicated for some specific job, improving predictability
     * of access times.
     * Example: Separate stacks for core0 and core1.
     */
    SRAM4 : ORIGIN = 0x20040000, LENGTH = 4k
    SRAM5 : ORIGIN = 0x20041000, LENGTH = 4k

    /* SRAM banks 0-3 can also be accessed directly. However, those ranges
       alias with the RAM mapping, above. So don't use them at the same time!
    SRAM0 : ORIGIN = 0x21000000, LENGTH = 64k
    SRAM1 : ORIGIN = 0x21010000, LENGTH = 64k
    SRAM2 : ORIGIN = 0x21020000, LENGTH = 64k
    SRAM3 : ORIGIN = 0x21030000, LENGTH = 64k
    */
}

EXTERN(BOOT2_FIRMWARE)

SECTIONS {
    /* ### Boot loader */
    .boot2 ORIGIN(BOOT2) :
    {
        KEEP(*(.boot2));
    } > BOOT2
} INSERT BEFORE .text;
