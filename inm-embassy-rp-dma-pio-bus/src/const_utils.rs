//! const-versions of utilities. This is mostly for the time stuff.

/// Put the larger value last and the smaller value first.
pub(crate) const fn const_minmax_u16([a, b]: [u16; 2]) -> [u16; 2] {
    if a <= b {
        [a, b]
    } else {
        [b, a]
    }
}

// Note that constants can't be used inside traits so we have to do specific functions for the
// various categories of [`fugit`] types rather than an extension type.
//
// Some of the fugit types have an accessible const_cmp directly, which we allow using with
// @cmp or @partial_cmp on the end
macro_rules! make_comparators {
    ($backing_ty:ident $fugit_generic_ty:ident $min_name:ident $max_name:ident $minmax_name:ident @$cmp_extractor:ident) => {
        // These docs with concat don't work :(
        // Best to avoid any documentation that's just weird, so we comment it all out :(

        /*
            #[doc = concat!(" const max function to find the maximum between two [`", stringify!($backing_ty), "`]-backed ")]
            #[doc = concat!(" [`fugit::", stringify!($fugit_generic_ty), "`]s with the same numerators and denominators")]
            /// Const functions can't be in extension traits, and the first type parameter of
            /// [`fugit`] types (the backing type) isn't implemented with any constraint, just unique
            /// implementations. As such we need different functions for each category of [`fugit`] type
            /// and backing store.
            */
        #[allow(dead_code)]
        pub(crate) const fn $max_name<const NOM: u32, const DENOM: u32>(
            a: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
            b: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
        ) -> fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM> {
            /*
                let Some(c) = a.const_partial_cmp(b) else {
                    panic!("should not be possible with identical nom/denom")
                };
                */
            make_comparators!(@$cmp_extractor c a b);
            match c {
                // Matches the normal [core::cmp::Ord::max] behaviour
                ::core::cmp::Ordering::Less | ::core::cmp::Ordering::Equal => b,
                ::core::cmp::Ordering::Greater => a,
            }
        }

        /*
            #[doc = concat!(" const min function to find the minimum between two [`", stringify!($backing_ty), "`]-backed ")]
            #[doc = concat!(" [`fugit::", stringify!($fugit_generic_ty), "`]s with the same numerators and denominators")]
            /// Const functions can't be in extension traits, and the first type parameter of
            /// [`fugit`] types (the backing type) isn't implemented with any constraint, just unique
            /// implementations. As such we need different functions for each category of [`fugit`] type
            /// and backing store.
            */

        #[allow(dead_code)]
        pub(crate) const fn $min_name<const NOM: u32, const DENOM: u32>(
            a: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
            b: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
        ) -> fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM> {
            make_comparators!(@$cmp_extractor c a b);
            match c {
                // Matches the normal [core::cmp::Ord::min] behaviour
                ::core::cmp::Ordering::Less | ::core::cmp::Ordering::Equal => a,
                ::core::cmp::Ordering::Greater => b,
            }
        }

        /*
            #[doc = concat!(" const minmax function to order two [`", stringify!($backing_ty), "`]-backed ")]
            #[doc = concat!(" [`fugit::", stringify!($fugit_generic_ty), "`]s with the same numerators and denominators, ")]
            #[doc = "from lowest to highest"]
            /// Const functions can't be in extension traits, and the first type parameter of
            /// [`fugit`] types (the backing type) isn't implemented with any constraint, just unique
            /// implementations. As such we need different functions for each category of [`fugit`] type
            /// and backing store.
            */
        #[allow(dead_code)]
        pub(crate) const fn $minmax_name<const NOM: u32, const DENOM: u32>(
            a: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
            b: ::fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>,
        ) -> [fugit::$fugit_generic_ty<$backing_ty, NOM, DENOM>; 2] {
            make_comparators!(@$cmp_extractor c a b);
            match c {
                // Matches the normal [core::cmp::minmax] behaviour, even if not? stabilised
                ::core::cmp::Ordering::Less | ::core::cmp::Ordering::Equal => [a, b],
                ::core::cmp::Ordering::Greater => [b, a],
            }
        }
    };
    (@cmp $out:ident $a:ident $b:ident) => { let $out = $a.const_cmp($b); };
    (@partial_cmp $out:ident $a:ident $b:ident) => { let Some($out) = $a.const_partial_cmp($b) else { panic!("should not be possible with identical nom/denom") }; }
}

make_comparators!(u32 Duration min_duration_u32 max_duration_u32 minmax_duration_u32 @partial_cmp);
make_comparators!(u64 Duration min_duration_u64 max_duration_u64 minmax_duration_u64 @partial_cmp);
make_comparators!(u32 Instant min_instant_u32 max_instant_u32 minmax_instant_u32 @cmp);
make_comparators!(u64 Instant min_instant_u64 max_instant_u64 minmax_instant_u64 @cmp);
make_comparators!(u32 Rate min_rate_u32 max_rate_u32 minmax_rate_u32 @partial_cmp);
make_comparators!(u64 Rate min_rate_u64 max_rate_u64 minmax_rate_u64 @partial_cmp);

/// const cmp for int types. Implemented using the same code as in [core], so it has the same
/// semantics, just const.   
pub(crate) mod const_cmp {
    macro_rules! make_cmp {
        ($($t:ident $cmp_fn:ident)*) => {
            $(
                #[inline]
                pub const fn $cmp_fn(s: &$t, other: &$t) -> ::core::cmp::Ordering {
                    // The order here is important to generate more optimal assembly.
                    // See <https://github.com/rust-lang/rust/issues/63758> for more info.
                    if *s < *other { ::core::cmp::Ordering::Less }
                    else if *s == *other { ::core::cmp::Ordering::Equal }
                    else { ::core::cmp::Ordering::Greater }
                }
            )*
        };
    }

    make_cmp!(
        char cmp_char usize cmp_usize u8 cmp_u8 u16 cmp_u16 u32 cmp_u32 u64 cmp_u64
        u128 cmp_u128 isize cmp_isize i8 cmp_i8 i16 cmp_i16 i32 cmp_i32 i64 cmp_i64
        i128 cmp_i128
    );
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
