//! Implementation for the bus with descriptor p8_1c1d1w1r8bc
//! (all adjacaent parallel 8-bit half-duplex bus with chip-select, data-or-command, write- and
//! read- control lines, and 8 data bus lines (in cis- order to hardware), all pins adjacent.
//!
//! Chip-select pins are distinct from all other pins, as they are managed via the [``]

use core::convert::Infallible;

use crate::{
    bus_modes::{CompiledModeData, GlobalAccessManager, GlobalModeProvider},
    const_utils,
    parallel::{
        chip_select::BusChipSelectBehaviour, half_duplex::with_data_command as control,
        width8 as data,
    },
    poa, NanosDuration,
};

use embassy_rp as hal;
use fixed::{
    traits::{Fixed, LosslessTryInto},
    types::extra::{U4, U8},
};
use fugit::RateExtU32 as _;
use hal::{pio::PioPin, Peripheral};
use pio::RP2040_MAX_PROGRAM_SIZE;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum BusConstructionError<CompileError> {
    /// Error when loading the program. May occur for a number of reasons (e.g. if the
    /// bank containing the supplied state machine has insufficient memory).
    PioLoadingError(hal::pio::LoadError),
    /// Difficult error to get with HAL's singleton protection, but may be possible with
    /// duplicated references or manual janking around with internal pin ids.
    InvalidPin(poa::PinBankError),
    /// The pins were not in the correct order for this bus implementation.
    WrongPinOrder(poa::PinBankClassification),
    /// Error when compiling the user mode specification into internal specification.
    CompileError(CompileError),
    /// Error converting from clock rate to clock cycle time. Probably some divide-by-zero
    /// thing.
    ClockCycleTime,
}

/// Type for valid pin ordering for this bus layout.
/// Can only be constructed through a method that validates the pins.
///
/// This doesn't include the chip select pins. Validation for those is separate and implied by the
/// way they are passed into the main structure.
pub struct ValidBusPins<'p, PIO: hal::pio::Instance + 'p> {
    main_pin_list: [hal::pio::Pin<'p, PIO>; 11],
}

// The need to configure
impl<'p, PIO: hal::pio::Instance + 'p> ValidBusPins<'p, PIO> {
    ///
    /// This function takes a set of pin banks and validates that they're in the arrangement
    /// required for the PIO program to work. As such, it configures the pins in a certain way:  
    /// * Input sync bypassing - the standard and config documents very well the exact timing required
    ///
    /// Furthermore, it also configures the GPIO chip-select mechanisms.
    pub fn validate_and_initialize_pins<E>(
        pio_block_common: &mut hal::pio::Common<'p, PIO>,
        control_pins: control::BusControlPins<
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
        >,
        data_pins: data::BusDataPins<
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
            impl Peripheral<P = impl PioPin> + 'p,
        >,
    ) -> Result<Self, BusConstructionError<E>> {
        let mut control_pins = control::BusControlPins::new(
            (),
            pio_block_common.make_pio_pin(control_pins.cs_rw_pins.read_control),
            pio_block_common.make_pio_pin(control_pins.cs_rw_pins.write_control),
            pio_block_common.make_pio_pin(control_pins.data_or_command_mode),
        );

        let mut data_pins = data::BusDataPins::new((
            pio_block_common.make_pio_pin(data_pins.pins.0),
            pio_block_common.make_pio_pin(data_pins.pins.1),
            pio_block_common.make_pio_pin(data_pins.pins.2),
            pio_block_common.make_pio_pin(data_pins.pins.3),
            pio_block_common.make_pio_pin(data_pins.pins.4),
            pio_block_common.make_pio_pin(data_pins.pins.5),
            pio_block_common.make_pio_pin(data_pins.pins.6),
            pio_block_common.make_pio_pin(data_pins.pins.7),
        ));

        // Note: in further generalization, this is where we would perform reordering
        // within a bank.
        let mut bus_ordering = [
            // control_pins.cs_rw_pins.chip_select,
            control_pins.data_or_command_mode,
            control_pins.cs_rw_pins.write_control,
            control_pins.cs_rw_pins.read_control,
            data_pins.pins.0,
            data_pins.pins.1,
            data_pins.pins.2,
            data_pins.pins.3,
            data_pins.pins.4,
            data_pins.pins.5,
            data_pins.pins.6,
            data_pins.pins.7,
        ];

        // validate that the bus ordering is the correct form for this bus implementation.
        match poa::classify_pio_pin_array(bus_ordering.iter())
            .map_err(BusConstructionError::InvalidPin)?
        {
            poa::PinBankClassification::CisAdjacent => {}
            invalid_ordering => return Err(BusConstructionError::WrongPinOrder(invalid_ordering)),
        };

        // disable input/output sync on the pins. We have very precise control over the
        // timing in PIO, so this is desirable to increase processing rate.
        bus_ordering
            .iter_mut()
            .for_each(|p| p.set_input_sync_bypass(true));

        Ok(Self {
            main_pin_list: bus_ordering,
        })
    }

    /// Pins in cis-order, as an array ref
    ///
    /// Note that no &mut can be obtained from this, as this would violate valid order
    /// constraints. Does not contain CS controls.
    pub const fn pins(&self) -> &[hal::pio::Pin<'p, PIO>; 11] {
        &self.main_pin_list
    }

    /// Pins in cis-order, as an array of refs. Does not include CS controls.
    pub const fn individual_pins(&self) -> [&hal::pio::Pin<'p, PIO>; 11] {
        let [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10] = &self.main_pin_list;
        [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10]
    }

    /// Control pins by-ref, except chip-selects (handled outside this type.
    pub const fn control_pins(&self) -> control::BusControlPins<(), &hal::pio::Pin<'p, PIO>> {
        let [data_or_command_mode, write_control, read_control, ..] = &self.main_pin_list;
        control::BusControlPins::new((), read_control, write_control, data_or_command_mode)
    }

    /// Data pins by-ref
    pub const fn data_pins(&self) -> data::UBusDataPins<&hal::pio::Pin<'p, PIO>> {
        let [.., d0, d1, d2, d3, d4, d5, d6, d7] = &self.main_pin_list;
        data::BusDataPins {
            pins: (d0, d1, d2, d3, d4, d5, d6, d7),
        }
    }

    /// How chip selection works on this set of valid bus pins.
    pub const fn chip_select_behaviour(&self) -> BusChipSelectBehaviour {
        self.chip_selection_behaviour
    }
}

/// Fractional representation used when talking about cycle delays.
pub type FractionalCycles = fixed::FixedU32<U4>;

/// Type of timing to actually use inside a pio program
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum CompiledBusTiming {
    /// PIO cycle delays and wait instructions should be used.
    ///
    /// The overwhelming majority of the time, this is what's desired and what will
    /// be used.
    ///
    /// The number includes the time of the instruction itself (which is always a single cycle). Of
    /// course this always works for zero-needed delay as well.
    ///
    /// Note that this provides cycles as a fractional value. This is so for very small - but
    /// nonzero - timings, we still allow merging in.
    Cycles {
        total_delay_needed_in_cycles: FractionalCycles,
    },
    /// Wait on the CPU for the given time.  
    CpuWait(NanosDuration),
}

impl CompiledBusTiming {
    pub const ZERO: Self = Self::Cycles {
        total_delay_needed_in_cycles: FractionalCycles::ZERO,
    };

    /// In this case no delay is needed. Oftentimes, this means that some action can be
    /// merged with others for less instructions.
    pub const fn is_zero(&self) -> bool {
        match self {
            CompiledBusTiming::Cycles {
                total_delay_needed_in_cycles,
            } => *total_delay_needed_in_cycles.is_zero(),
            CompiledBusTiming::CpuWait(duration) => duration.is_zero(),
        }
    }
}

/// Raw representation of the PIO clock divider.
///
/// See [`PIOClockDivider`] for notes on the actual representational requirements.
pub type PIOClockDividerRepr = fixed::FixedU32<U8>;

/// Fractional clock cycle divider to use for the PIO program.
///
/// It also can't have ints > u16 (i.e the integer
/// part must be at most 65535). An integer value of 0 gets interpreted as 65536 (presumably the
/// fractional part gets added on to that or something, though at those scales the fractional
/// component is very insignificant).
#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct PIOClockDivider(PIOClockDividerRepr);

impl PIOClockDivider {
    pub const ONE: Self = Self(PIOClockDividerRepr::ONE);

    /// Maximum allowed value is 65536 + max fractional bits, which is 65537 - 1-delta. 65536 is
    /// represented as zero in the integer bits.  
    const MAX_ALLOWED_VALUE: PIOClockDividerRepr =
        PIOClockDividerRepr::from_num(u16::MAX as u32 + 2).dist(PIOClockDividerRepr::DELTA);

    /// Value that must be subtracted from a value >= itself but less than max to get the actual
    /// representation.
    ///
    /// This is just the representation of `65536`, which is the one integer component that is
    /// represented as zeros. Adding more than that results in > the max allowed value and that
    /// gives errors.
    const MIN_CIRCULAR_REPRESENTATION: PIOClockDividerRepr =
        PIOClockDividerRepr::from_num(u16::MAX as u32 + 1);

    /// Construct a PIO clock divider. Note that this will convert the value to the representation
    /// needed by PIO internally.
    ///
    /// Any values < 1 will cause returning of [`None`], and any values > 65536 (+ allowed
    /// fractional bits) will result in [`None`]
    pub const fn new(clock_divider: PIOClockDividerRepr) -> Option<Self> {
        if clock_divider < PIOClockDividerRepr::ONE || clock_divider > Self::MAX_ALLOWED_VALUE {
            return None;
        }
        let representation = if clock_divider >= Self::MIN_CIRCULAR_REPRESENTATION {
            clock_divider.abs_diff(Self::MIN_CIRCULAR_REPRESENTATION)
        } else {
            clock_divider
        };

        Some(Self(representation))
    }
}

/// Compiled data on how to construct the PIO program with precise timings.
///
/// This is compiled in the sense that provided timings have been converted into some kind of fixed
/// clock divider + raw PIO instruction cycles.
///
/// This also accounts for the option where something might be so slow that it needs to call back
/// to the cpu or something to get a delay (as in, request a delay). This is a very unlikely
/// scenario, but may apply in the case where timings for a single device mode have differences of
/// multiple orders of magnitude such that putting simple WAIT instructions inside the PIO program
/// is nonviable.
///
/// Multiple methods exist to select bus modes. There are ones which will not tolerate the need for
/// IRQ (and hence need no irq arguments). However, for the case where you might want to allow irq for
/// timing reasons, they will take one PIO IRQ as input. Then, the program will allocate a small
/// array containing the irq-needed delays, and when the pio program wants to delay that long it
/// will emit the irq with the index of the time delay it needs in one of the registers, then, wait
/// for the irq to be retriggered.
///
/// Right now, this doesn't deal specially with the case of data command control switching separate
/// minimum delay from the standard reset time for reads/writes (i.e. the delay for d/c switching
/// is included in all read/write delays). In future, we might make this more efficient.
pub struct CompiledBusData {
    /// Precise way to control the read/write/data pins.
    control_behaviours: control::BusControlBehaviour<()>,

    /// We always prefer to go as fast as possible, but if the inserted instruction delays become >
    /// the limited available number of cycles, then we will be forced to slow down the clock
    /// instead.
    ///
    /// Basically, this should be 1 when we can.     
    fractional_clock_divider: PIOClockDivider,

    /// Equivalent to [`control::BusTimings::read_ctrl_min_initiating_pulse_length`]
    read_ctrl_min_initiating_pulse_cycles: CompiledBusTiming,

    /// Equivalent to [`control::BusTimings::min_reset_time`]
    read_ctrl_min_reset_cycles: CompiledBusTiming,

    /// Like [`control::BusTimings::data_command_ctrl_min_switch_pre_r_wait_time`]
    data_command_ctrl_min_switch_pre_r_cycles: CompiledBusTiming,

    /// Like [`control::BusTimings::data_command_ctrl_min_switch_pre_w_wait_time`]
    data_command_ctrl_min_switch_pre_w_cycles: CompiledBusTiming,

    /// Equivalent to [`control::BusTimings::write_ctrl_min_initiating_pulse_length`]
    write_ctrl_min_initiating_pulse_cycles: CompiledBusTiming,

    /// Equivalent to [`control::BusTimings::write_ctrl_min_reset_time`]
    write_ctrl_min_reset_cycles: CompiledBusTiming,

    /// Side-set configuration
    side_set_cfg: pio::SideSet,
}

impl CompiledBusData {
    /// Create a version of this where all timings are zero-delay (and the fractional clock
    /// divider is 1).
    pub const fn zero(
        controls: control::BusControlBehaviour<()>,
        side_set_cfg: pio::SideSet,
    ) -> Self {
        CompiledBusData {
            control_behaviours: controls,
            fractional_clock_divider: PIOClockDivider::ONE,
            read_ctrl_min_initiating_pulse_cycles: CompiledBusTiming::ZERO,
            read_ctrl_min_reset_cycles: CompiledBusTiming::ZERO,
            data_command_ctrl_min_switch_pre_r_cycles: CompiledBusTiming::ZERO,
            data_command_ctrl_min_switch_pre_w_cycles: CompiledBusTiming::ZERO,
            write_ctrl_min_initiating_pulse_cycles: CompiledBusTiming::ZERO,
            write_ctrl_min_reset_cycles: CompiledBusTiming::ZERO,
            side_set_cfg,
        }
    }
}

/// Optimization tuning parameters for when constructing the PIO program for the bus.
///
/// These represent how the bus implementation should manage things like the PIO clock divider to
/// minimize the used PIO instruction memory, and to avoid CPU-based waits.  
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct OptimizationParameters {
    /// Maximum tolerable # of wait instructions beyond the intrinsic wait parameter in each PIO
    /// instruction before we switch to using [`CompiledBusTiming::CpuWait`].
    ///
    /// As the # of available bits to describe the wait parameter of instructions is dependent on the
    /// number of bits in the side set, this will depend also on `side_set_config` used by the bus.
    ///
    /// Note that the actual PIO WAIT instruction waits on things like input or registers. When we
    /// say "wait" instructions, what we really mean is use an instruction with no effect
    /// (e.g. `mov y y`, shorthand `nop`)
    ///
    /// Recommended value is `3`
    pub max_tolerable_extra_wait_instructions: u8,

    /// Maximum slowdown of the fastest delay tolerated to avoid CPU waiting due to excess wait
    /// instructions. Recommended to be `1` (no tolerated delay). This is a multiplier fraction
    /// (values < 1 are increased to 1).
    pub max_cpu_avoidance_slowdown_multiplier: fixed::FixedU8<U4>,
}

/// Sensible defaults.
impl Default for OptimizationParameters {
    fn default() -> Self {
        Self {
            max_tolerable_extra_wait_instructions: 3,
            max_cpu_avoidance_slowdown_multiplier: fixed::FixedU8::<U4>::ONE,
        }
    }
}

/// Side data needed when compiling to [`CompiledBusData`]
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct RuntimeCompilationParameters {
    /// Duration of one single cycle of the core pio clock (pre clock-divider).
    pub pio_single_cycle_duration: NanosDuration,

    /// Side set configuration
    ///
    /// Note that this implicitly contains a set of sideset values (and also a boolean indicating
    /// whether or not an "enable/disable" bit has been turned on for sideset, which reduces the
    /// max sideset pins by 1). We don't care about that, only the # of bits used.
    pub side_set_cfg: pio::SideSet,

    /// Parameters for optimization
    pub opt_params: OptimizationParameters,
}

impl RuntimeCompilationParameters {
    /// Maximum # of cycles beyond the single instruction itself that can be encoded in the delay
    /// slot.
    pub fn delay_max(&self) -> u8 {
        let side_set_bits = self.side_set_cfg.bits();
        // 5 is the # of instruction bits allocated to delay + sideset.
        let delay_bits = 5 - side_set_bits;
        let delay_max = (1u8 << delay_bits) - 1;
        delay_max
    }
}

impl CompiledModeData<control::BusControlData<()>> for CompiledBusData {
    type Error = Infallible;

    type RuntimeInformation = RuntimeCompilationParameters;

    fn compiled_from(
        user_specified: control::BusControlData<()>,
        rt_timing_info: Self::RuntimeInformation,
    ) -> Result<Self, Self::Error> {
        // + 1 for the actual delay from running the instruction itself.
        let max_delay_per_instruction_pio_cycles: u32 = rt_timing_info.delay_max() + 1;

        // + 1 because delay can be injected into the actual instruction to be waited after too.
        let max_delay_pio_cycles_before_cpu_fallback = max_delay_per_instruction_pio_cycles
            * (rt_timing_info
                .opt_params
                .max_tolerable_extra_wait_instructions as u32
                + 1);

        // Cycle length of the raw undivided pio clock.
        let undivided_pio_cycle = rt_timing_info.pio_single_cycle_duration;
        // Get the minimum and maximum nonzero timings. If all timings are zero, then things are
        // very weird, but we return a set of valid timings for that anyhow.
        let [minimum_nonzero_delay, maximum_nonzero_delay] = match user_specified
            .timings
            .minmax_timings_including(|v| !v.is_zero())
        {
            Some(min_max) => min_max,
            None => {
                return Ok(Self::zero(
                    user_specified.control_behaviours,
                    rt_timing_info.side_set_cfg,
                ))
            }
        };

        /// Ratios for numeric manipulation in compilation.  
        type FixedRatio = fixed::FixedU64<fixed::types::extra::U32>;
        type FixedInt32 = fixed::FixedU32<fixed::types::extra::U0>;

        /// Raw delay ratio, num/denom.  
        const fn delay_ratio(num: NanosDuration, denom: NanosDuration) -> FixedRatio {
            FixedInt32::from_num(num.ticks()).wide_div(FixedInt32::from_num(denom.ticks()))
        }

        // On timing - We want to optimise 3 aspects:
        // * Using a small PIO clock divider - this allows us to perform more internal operations
        //   per cycle (highest priority), and reduces the risk of going slower than the device
        //   allows.
        // * Reduced instruction count - this means that if it is possible to merge instructions,
        //   it should be done (second priority)
        // * Reduced extra waiting - the more extra waiting glued on to an instruction or in
        //   multiple instructions, the greater the risk of falling back to CPU waiting, which is
        //   bad. (lowest priority).
        //
        // The smaller the PIO clock divider, the greater the risk of a needed delay falling back
        // to CPU. We want reduce the clock divider, but only so much that it prevents CPU waiting
        // and no further. However, if there is a really big delay, we don't want to bottleneck
        // everything else on it, so if the ratio is bigger than a certain amount we don't want to
        // slow everything else down to avoid cpu waiting.

        // Minimum delay as a fractional multiple of PIO cycles
        let minimum_nonzero_wait_pio_cycles =
            delay_ratio(minimum_nonzero_delay, undivided_pio_cycle);
        let maximum_nonzero_wait_pio_cycles =
            delay_ratio(maximum_nonzero_delay, undivided_pio_cycle);

        // If the ratio is less than
    }
}

/// 8-Bit bidirectional half-duplex data bus with control pins.
pub struct Bus<
    'p: 'sm,
    'sm,
    PIO: hal::pio::Instance,
    const STATE_MACHINE_NR: usize,
    CSControls,
    CSBehaviour,
    DeviceModeConfigurations,
> {
    pio_control: &'sm mut hal::pio::StateMachine<'p, PIO, STATE_MACHINE_NR>,
    validated_pins: ValidBusPins<'p, PIO>,
    /// Mode structure and managed chip-select mechanism
    mode_and_accessor_management:
        GlobalAccessManager<CSControls, DeviceModeConfigurations, CSBehaviour>,
}

impl<
        'p: 'sm,
        'sm,
        PIO: hal::pio::Instance,
        const STATE_MACHINE_NR: usize,
        CSControls,
        CSBehaviour,
        DeviceModeConfigurations,
    > Bus<'p, 'sm, PIO, STATE_MACHINE_NR, CSControls, CSBehaviour, DeviceModeConfigurations>
{
    /// Construct a bus using pins matching the adjacency specification p8ng0cge1d1w1r8bc
    ///
    /// For information on that shorthand, see the main documentation of the crate.
    pub fn new<UserSpecifiedModeData: GlobalModeProvider>(
        pio_handle: impl Peripheral<P = PIO> + 'p,
        valid_pins: ValidBusPins<'p, PIO>,
        control_behaviour: control::BusControlBehaviour<()>,
        mode_and_chip_select_controls: GlobalAccessManager<
            CSControls,
            UserSpecifiedModeData,
            CSBehaviour,
        >,
        pio_block_common: &mut hal::pio::Common<'p, PIO>,
        pio_state_machine: &'sm mut hal::pio::StateMachine<'p, PIO, STATE_MACHINE_NR>,
    ) -> Result<
        Self,
        BusConstructionError<
            <CompiledBusData as CompiledModeData<UserSpecifiedModeData::ModeData>>::Error,
        >,
    >
    where
        CompiledBusData:
            CompiledModeData<UserSpecifiedModeData::ModeData, RuntimeInformation = fugit::HertzU32>,
    {
        let pio_clock: fugit::HertzU32 = hal::clocks::clk_sys_freq().Hz();

        // Time for a single cycle of the clock that - i'm pretty sure - drives the PIO.
        let pio_cycle_time =
            NanosDuration::try_from_rate(pio_clock).ok_or(BusConstructionError::ClockCycleTime)?;

        pio_state_machine.set_enable(false);

        let mut a = pio::Assembler::<RP2040_MAX_PROGRAM_SIZE>::new();
        // let pin_config = hal::pio::PinConfig::default().trans_mut(|c| c);
        todo!();
    }

    /// Get the way this particular bus implementation controls the chip select pins.
    #[inline]
    pub fn chip_select_behaviour(&self) -> &CSBehaviour {
        self.mode_and_accessor_management.chip_select_behaviour()
    }

    /// Select a device on the bus using one of the chip-select mechanisms.
    pub async fn claim_bus(&mut self) -> ActiveBus<'_, 'p, 'sm, PIO, STATE_MACHINE_NR> {
        let chip_select_pin = self.pio_control.set_pins(level, pins);
    }
}

pub struct ActiveBus<'b, 'p: 'sm, 'sm, PIO: hal::pio::Instance, const STATE_MACHINE_NR: usize> {
    bus: &'b mut Bus<'p, 'sm, PIO, STATE_MACHINE_NR>,
}

// Take a reference to the bus which selects the chip-select thing

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
