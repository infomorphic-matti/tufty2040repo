//! This is a library to construct efficient output (and maybe input) buses using the
//! GPIO pins as well as DMA and PIO on the RP2040 chip can be used as a form of parallel bus.
//!
//! This library requires nightly.
//!
//! Currently it mostly exists to drive the ST7789v on the tufty2040, but it's structured at least somewhat
//! generically that it could have more impls.
//!
//! ## MODULE LAYOUT
//! The more generic structures and information are located in fully named modules. For instance,
//! [`parallel::width8`] contains things related to buses with 8 bits of data, and
//! [`parallel::half_duplex`] contains information and structure related to all parallel
//! half-duplex buses (typically, control pin information). Within that, there is a submodule for
//! half-duplex buses which have an extra data/command selection control pin.  
//!
//! Because there is clearly an issue with a given bus type fitting into many modules, we use
//! separate modules for specific bus implementations. These all go into the [`bus`] module, with
//! named submodules of a certain structure. Modules now use solely the shorthand version of the
//! name described below.
//!
//! These names are of a specific format that indicates how the bus is organised. The name looks
//! like:
//! {parallel-ness}_{directionality}_{data-width-in-bits}[{endianness}]_{extra-control-pins}_{pin-ordering-and-adjacency}
//!
//! There is also a shorthand described near the end. Note that - from now on - all modules will be
//! nnamed directly via the shorthand, with a `_` instead of the n (to split fine arrangement from
//! coarse data format).
//!
//! Current values for these are as followed.
//!
//! parallel-ness:
//! * `p` - parallel data bus
//! * `s` - serial data bus
//!
//! directionality:
//! * ws - write (simplex)
//! * rs - read (simplex)
//! * hd - half-duplex (read or write but not simultaneously)
//! * fd - full-duplex (read and write simultaneously)
//!
//! data-width-in-bits:
//! * 4 - 4 bits of data
//! * 8 - 8 bits of data
//! * 16 - 16 bits of data
//! * <n> - *n* bits of data
//! * For >8 bits, you should add an endianness (e.g. `16le`, `48be`)   
//!     * this is an optional indicator that exists for buses with a > 8 bit data width. It indicates
//!       the endianness of the input/output data for these buses.
//!     * `le` - little-endian - the least significant byte is associated with the "first" 8 bits in the
//!       data bus pin structure (bits are rearranged further as per the ordering and adjacency descriptor).
//!     * `le` - big-endian - the most significant byte is associated with the "first" 8 bits in the
//!       data bus pin structure listed (bits are rearranged further as per the ordering and adjacency
//!       descriptor).
//! * For duplex (either half or full), this can be replaced with `w<n>r<m>`, which indicate
//!   that the chunk size for this is different on write (transmit) or read (receive). If not
//!   specified separately, then the value is assumed to be the same. `<n>` and `<m>` should
//!   include an endianness.
//! * This refers strictly to the amount sent/received in one go. The actual number of data pins is
//!   indicated by the ordering and adjacency specifier.
//!
//! extra-control-bits (composed of several letters each indicating one control pin, in the
//! following order, with `z` as a letter to increase the namespace):
//! * `n` - for cases where there are no control pins, should not be present otherwise.
//! * for parallel bus types:
//!     * `c` - for a chip-select pin
//!     * `d` - for a data/command selection control pin
//!     * `r` - for a read-control pin
//!     * `w` - for a write-control pin
//!     * `zc` - for a clock pin
//! * for serial bus types:
//!     * `zc` - for a clock pin
//!     * Probably others (right now this library doesn't have serial infrastructure but if I get
//!       more familiar with that then I will add letters for typical control pins).
//!
//! pin-ordering-and-adjacency is described below
//!
//! endianness:
//!
//! ### pin-ordering-and-adjacency
//! This is related to whether and how the pins need to be adjacent and how ordered
//! they need to be in comparison to the hardware ordering.
//!
//! When dealing with PIO, it is often easiest and most efficient to manage pins that are next to
//! each other on the chip (sometimes it loops). Furthermore, output of bits to a lot of pins
//! is often only possible in a single cycle when the data is written directly to adjacent pins in
//! a certain order that is hardware defined.
//!
//! The pin ordering and adjacency component of the module name is structured to encode every bank
//! of pins with a distinct ordering or which are separate or adjacent from each other.
//!
//! This component is read left to right, and a sequence of pins without an explicit separator
//! between them should be interpreted as needing to be in hardware order and adjacent.
//!
//! Data chunks are indicated by `<bitlength>b<c|t>`, where <bitlength> is a number (and mandatory):
//!  * `c` = cis-ordering: The lowest identified hardware pin within this part of the data bus
//!  sends/receives the least significant ("first") bit in the region of the data chunk to be sent/received
//!  over this part of the data bus.
//!  * `t` = trans-ordering: The highest identified hardware pin within this part of the data bus
//!  sends/receives the most significant ("last") in the region of the data chunk to be sent/received
//!  over this part of the data bus.
//!  * In the case of full-duplex, the `<length>b<c|t>` is instead `<length>bw<c|t>` or
//!    `<length>br<c|t>` to indicate whether or not the bank is for reading (receiving) or writing
//!    (sending).
//!    * The mandatoryness of the length data means that it cannot be ambiguous whether you mean a
//!      read-control bit or a data bus bit.
//!    * A theoretical bus where some parts were half-duplex and others full-duplex could have a mix
//!      of `b`, `bw` and `br` pins.
//!    * For coherency's sake, we also say that simplex buses should use `br` or `bw` depending on
//!      their direction.
//!    * There is a rare theoretical case where a bank of `b` pins might send data in `c`
//!      order, but receive it in `t` order (or vice-versa). To account for this, we allow specifying
//!      `c` and `t` twice optionally (like `bct`, `bcc`, `btt`), where the first indicates how data
//!      is received and second indicates how it is sent. A single-order `bc`/`bt` is equivalent to
//!      `bcc`/`btt`, respectively.
//!  * If we have a type of bus with multiple control bits of the same type, they use a
//!    similar system but with `b` replaced directly by the control pin letter. `c`/`t` then refers
//!    to how the "id"s of those pins (in particular chip-select pins) are picked out from the
//!    hardware pins for triggering. `t` means that lower hardware-pins have higher identifiers,
//!    whereas `c` means that lower hardware pins have lower identifiers.
//!  * A length of `0` indicates that the actual length is defined via some kind of type
//!    parameter/generic stuff. This is implemented using [`bus_modes::GlobalModeProvider`].
//!    For pins where their configuration involves some kind of order (as opposed to things like
//!    chip-select where one is "enabled" and any others are "disabled"), a c or t ordering is
//!    required.
//!  * Similarly, a generics-controlled ordering of a subset of pins or pin collections can
//!    be indicated by surrounding the reorderable bank by `g` and `ge` (for `generic` and
//!    `generic-end`). In this case, `c` and `t` indicators are still useful as whatever
//!    rearrangement is applied by the generic should be applied "after" the `c`/`t` rearrangement
//!    of data. It can apply to any subset of the pin descriptors e.g. `g2rc1wge8bc` would have a
//!    generically rearrangable collection of (2 read controllers, 1 write controller) that must be
//!    adjacent to an 8 bit data bus.
//!
//! Control pins are indicated by the same mechanism, but instead of `b`, the letter from
//! `extra-control-bits` is used. They still have mandatory length descriptors.
//!
//! A bit length of 1 does not require a `c` or `t` indicator, as it doesn't represent a
//! collection of data/control bits and pins, but only one (and as such has no order than
//! it's place in the bus).
//!
//! There are three types of separators that indicate that the sequence of pin descriptors after this
//! does or doesn't need to be hardware-adjacent to the previous pins:
//! * `a` - a dummy pin name that still indicates a need for adjacency (basically it exists solely
//!    to let the name be split up into logical subsections for some clarity of reading).
//!
//!    Often this might go between a bank of control pin descriptors and a bank of data pin descriptors,
//!    or in particularly weird cases it might be used to make the boundary of a pin's descriptor
//!    clear (I think the encoding i've described is unambiguous but it might *look* ambiguous
//!    sometimes, and I might be wrong).
//!
//!    Within a `g`/`ge` pair, this carries extra meaning/constraint.
//! * `p` - indicates a "partial"/"ordered" split. The pins after this do not have to be adjacent to the pins
//!   before this, but they still need to be further along in hardware order (note that the RP2040
//!   has pin ordering with overflow, so this is more flexible than it might seem). However, it
//!   only has 30 of the 32 possible GPIOs, so there are some situations where looping is
//!   disallowed unless you go far enough.
//! * `f` - indicates a "full" split. The pins after this do not have to be adjacent to the
//!    previous chunks of pins and they don't have to be "after" the previous chunk in the hardware
//!    ordering either. Regardless of the "real" order of the pins on hardware, when it comes to
//!    `c`/`t` indicators or the endianness indicators, those apply "as-if" the pins are after the
//!    previous chunk. It's just that by default, the pin arrangement descriptors
//!
//! When it comes to the expression of generic-ordering-parameterised structures (`g`/`ge`), the question of
//! these adjacencies is more complex. By default, all elements inside `g`/`ge` can be reordered at
//! will, but the overall bank must be fully adjacent/contiguous.
//!
//! Every `p` or `f` within a `g`/`ge` pair creates a point of division. The rearrangement then allows for
//! rearranging pins within the generic specifier between the split banks of the width of the pins specified
//! within each sub-segment. For instance `g2ccf1r1w1dge` has one length-2 bank and 1 length-3 bank, which the 2c/1r/1w/1d
//! control pins can be distributed across.
//!
//! Note that if you want two separate banks which can only be rearranged within themselves, that's easily done by just
//! splitting up into two generic `g`/`ge` things.
//!
//! `a` is "stronger" than `p` is "stronger" than `f`. That is, `<bank0>p<bank1>f<bank2>p<bank3>` means
//! that the `f` applies to the entire pair of `<bank0>p<bank1>` and `<bank2>p<bank3>`. So the `p`
//! constraint on `<bank3>`'s position only applies relative to `<bank2>`, not the other two banks.
//! Similarly, for `1c1dp1r1b`, the constraints of `a` (which is implicitly "between" each pin bank
//! definition on each side, like `1ca1dap1ra1b`) only apply between `1r1b` and `1c1d`, not between
//! `1d` and `1b`.
//!
//! #### pin-adjacency-and-ordering examples
//! The complexity of the naming system for bus types/modules/etc. means it's worth seeing some
//! examples of the adjacency and ordering component. Each of these should be assumed to be
//! parallel (for now).  
//! * `0ct1r1w8bt` indicates an 8 bit bus with a generic number of chip-select pins (where the IDs
//!   are larger for lower hardware pins). All the pins must be adjacent and in hardware order. The
//!   data is transmitted/received with more significant bits on lower hardware pins.
//! * `1r1wf4brcp4bwt` is a bidirectional bus with 4 bit lines in each direction:
//!   * With 1 read-control line and 1 write control line (which must be in hardware order and adjacent to
//!      each other).
//!   * The data lines can be anywhere in the hardware pins relative to the control lines, but the
//!      write-lines must be after the read-lines in terms of hardware pins (however they can be anywhere
//!      after, they aren't required to be adjacent)
//!   * The read lines read the least significant ("first") bit from the lowest hardware pins.
//!     However, the write lines are of opposite ordering and the least significant ("first") bit
//!     is written on the highest hardware-pins.
//! * `1c1r1w1d8bc` is a half-duplex bus with a chip select pin, read control pin, and write
//!   control pin, and data-command pin, and then an 8 bit data channel where the lowest
//!   hardware-pin transmits or receives the least-significant-bit ("first" bit). All of them must
//!   be in hardware order and adjacent.
//!
//! ### Possible Shorthand
//! There is a possible shorthand method for describing the interface and structure of a bus.
//! Important things to note for this:
//!  * Whether a bus is simplex-send, simplex-receive, half-duplex, or full-duplex (or some mix),
//!    can be derived entirely from the `b`/`br`/`bw` pins used (though if there is a bus with
//!    multiple of certain control lines like r/w/d/c/cs, the implementation would need to define
//!    how it actually connected them with data)
//! * The control pins used in the bus can be derived entirely from the pin arrangement as well
//! * The only determinant of control pin meanings is whether the bus is parallel or serial. As
//!   such, we can prefix with `p` or `s`.
//! * We just need to add a separator between the data width indicator and the pin arrangement
//!   indicator. The code for "no control pins" (`n`) works well as a replacement.
//! * As such, we can then describe a fair amount of a bus implementation's structure and
//!   requirements using a shorthand like the following: `<p|s><data-width-specifier[le|be]>n<pin-arrangement>`
//! * Example for the tufty2040 (if the implementation requires full adjacency and ordering): `p8n1c1d1w1r8dc`
//! * Example for a full duplex nibble bus with read and write and no chip-select: `p4n1w1r4bwc4brc`
//! * Example for a 32 bit simplex bus in trans-order (the on-the-line ordering is BE with "flipped" bytes, but
//!   the pin arrangement is such that the bus appears to send little-endian - bit arrangement is for the
//!   actual hardware arrangement, the LE/BE is for turning the data chunks into locations to then
//!   be rearranged as per bit ordering): `p32len1w32bwt`
//!
//! ## Extra Info
//! Here we implement the general idea of that, with a parallel bus in the form used by the ST7789v
//! (datasheet in the repo or online), in parallel 8 bit mode (as indicated by IM0/1/2/3 on the
//! display being pulled to ground, documented in the tufty2040 diagram), plus a few bus control
//! pins.
//!
//! A lot of use has been made of the [pimonori ST7789 driver](https://github.com/pimoroni/pimoroni-pico/tree/main/drivers/st7789)
//! to determine the actually relevant pins for a bus, because the tufty2040 schematic (including
//! the documentation of how it connects with the LCD pins) doesn't perfectly match up with the pin
//! names in the ST7789 datasheet. It was very helpful in determining the most relevant
//! bus-components.
#![no_std]
#![feature(generic_const_exprs)]

pub(crate) mod const_utils;
pub mod hardware_constants;

pub mod pin_ordering_adjacency;
pub use pin_ordering_adjacency as poa;


/// All parallel bus stuff
pub mod parallel {
    pub mod chip_select; 
    
    pub mod width8;

    pub mod half_duplex;
}

pub mod bus {
    pub mod p8_g0cge1d1w1r8bc;
}

pub mod bus_modes;

pub type NanoRepr = u32;
pub type NanosDuration = fugit::NanosDuration<NanoRepr>;

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
