//! Module that ensures that [`crate::parallel::chip_select::ChipSelectBehaviour`]s are used
//! properly, using lifetimes to ensure that only a maximum of one chip is ever selected at a time.
//! from a collection of devices on the same bus.

use crate::parallel::chip_select::ChipSelectBehaviour;

use super::{hardware_control::ControlMechanism, GlobalSelectionMechanisms};

/// Indicates the state in which all devices in [`Selectors`] (which should implement
/// [`GlobalSelectionMechanisms`] to be useful) are unselected as per the CS behaviour.
///
/// Then, when you have this, you can hand out RAII handles to specific chips on the bus.
pub struct AllUnselected<Selectors, CSBehaviour> {
    cs_control_mechanisms: Selectors,
    cs_behaviour: CSBehaviour,
}

impl <Selectors, CSBehaviour> AllUnselected<Selectors, CSBehaviour> {
    /// Recover the internal contents.
    pub const fn decompose(self) -> (Selectors, CSBehaviour) {
        (self.cs_control_mechanisms, self.cs_behaviour) 
    }

    /// Get the chip select behaviour
    pub const fn chip_select_behaviour(&self) -> &CSBehaviour {
        &self.cs_behaviour
    }
}

impl<
        'controls,
        CSControl: ControlMechanism,
        Selectors: GlobalSelectionMechanisms<'controls, CSControl>,
        CSBehaviour: ChipSelectBehaviour<CSControl>,
    > AllUnselected<Selectors, CSBehaviour>
{
    /// Initialize this "no devices selected" state
    ///
    /// # SAFETY
    /// You must ensure that the provided chip-select behaviour is the real chip-select behaviour
    /// for all devices on the bus.
    pub unsafe fn new(
        mut selectors: Selectors,
        behaviour: CSBehaviour,
    ) -> Result<Self, CSBehaviour::Error> {
        behaviour.disable_all_devices(&mut selectors)?;

        Ok(Self {
            cs_control_mechanisms: selectors,
            cs_behaviour: behaviour,
        })
    }

    /// Select a device by identifier.
    pub fn select_device(
        &mut self,
        device: &Selectors::ChipIdentifier,
    ) -> Result<SelectedChipHandle<'_, 'controls, CSControl, CSBehaviour>, CSBehaviour::Error> {
        // Safety:
        // &mut self ensures that this is the only access to the control mechanisms - so while the
        // selected chip handle holds a reference to self, we can know that noone else will
        // activate another device.
        // SelectedChipHandle always unselects a device on destruction too.
        unsafe {
            self.cs_control_mechanisms.apply_to_device(device, |dev| {
                SelectedChipHandle::new_raw(dev, &self.cs_behaviour)
            })
        }
    }
}

/// RAII handle that disables a chip when dropped using an implementation of [`ChipSelectBehaviour`]
///
/// More sophisticated usage can be found in the [`ChipSelectBehaviourExt`] extension trait.
pub struct SelectedChipHandle<
    'chip,
    'controls: 'chip,
    CSMechanism: ControlMechanism,
    CSBehaviour: ChipSelectBehaviour<CSBehaviour>,
> {
    cs_behaviour: &'chip CSBehaviour,
    cs_handle: CSMechanism::TemporaryExclusiveAccess<'chip, 'controls>,
}

impl<
        'chip,
        'controls: 'chip,
        CSMechanism: ControlMechanism,
        CSBehaviour: ChipSelectBehaviour<CSMechanism>,
    > SelectedChipHandle<'chip, 'controls, CSMechanism, CSBehaviour>
{
    /// Select a chip and hold on to it.
    /// [`AllUnselected`] provides other sophisticated methods of using this structure.
    ///
    /// # SAFETY
    /// You must ensure that no other device on the same collection of chip-select pins is selected
    /// at the same time.
    pub unsafe fn new_raw(
        mut device: CSMechanism::TemporaryExclusiveAccess<'chip, 'controls>,
        behaviour: &'chip CSBehaviour,
    ) -> Result<Self, CSBehaviour::Error> {
        behaviour.enable_device_raw(&mut device)?;
        Ok(Self {
            cs_behaviour: behaviour,
            cs_handle: device,
        })
    }
}

impl<
        'chip,
        'controls: 'chip,
        CSMechanism: ControlMechanism,
        CSBehaviour: ChipSelectBehaviour<CSMechanism>,
    > Drop for SelectedChipHandle<'chip, 'controls, CSMechanism, CSBehaviour>
{
    fn drop(&mut self) {
        unsafe {
            self.cs_behaviour.disable_device_raw(&mut self.cs_handle);
        }
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient uses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>} This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
