//! Module containing representations of different control mechanisms that can be used for some
//! component of a system (a bus, in this crate).
//!
//! Typically, a trait describing a behaviour may be parameterised on one or more
//! [`crate::bus_modes::hardware_control::ControlMechanism`], which allows a given implementation
//! to implement specifically which control mechanisms it can work on. Usually, each
//! implementation will have constraints that allow it to turn some internal representation into
//! the uniform [`ControlMechanism::ExclusiveAccess`].
//!
//! Other components that are generic over the control mechanism can hold on to the exclusive
//! access handle regardless of how it was obtained. In many ways, this is a generalisation over
//! various specific control mechanisms (like [`hal::PeripheralRef<'_, hal::gpio::AnyPin>`] or
//! [`hal::pio::Pin<'_, PIO>`]).
//!
//! Long term goals may be to merge this with some aspects of
//! [`crate::parallel::half_duplex::BusChipSelectBehaviour`] and similar things for various other
//! bus components. This would allow massively increased modularity, to the point where we could
//! completely detach the hardware layout of a bus from the control mechanisms for individual or
//! bulk pins, as well as potentially separate out timing behaviour. This would then allow
//! protocol implementations to be separated from the bus layout (and with async fn in traits being
//! in rust, this would enable significant abstraction and integration with async stuff).

use super::hal;
use core::marker::PhantomData;

/// Specify that some implementation can control some part of it's internal processes via a uniform
/// control mechanism.
pub trait ControlMechanism {
    /// Means of controlling the mechanism or process. Typically a uniform type (e.g.
    /// [`hal::PeripheralRef<'q, hal::gpio::AnyPin>`]), but could be some specific hardware.
    ///
    /// Note that this is very flexible. In theory, in future, this could be implemented on dyn
    /// Trait things, or something else, and allow being generic over various hardware
    /// capabilities using things like `embedded-hal`.
    ///
    /// The only problem is that this loses some small amount of efficiency. Unfortunately until
    /// Rust gets generics of arbitrary parameter count (a-la C++), this uniform representation is
    /// the price paid for not having tons and tons of things with tuples as type parameters (we
    /// may shift to this in future if we figure out a good API, maybe allow structures to
    /// implement some type indicating the hardware singleton types it contains and then doing
    /// wrapper types that only work when all parts implement GPIO/Pio/etc?). 
    type ExclusiveAccess<'q>;

    /// A lifetime-shortened means of accessing [`ControlMechanism::ExclusiveAccess`].
    /// Typically this is equivalent to a reborrowed &mut, but at the very least the embassy
    /// HAL has a more efficient form of storage since lots of the types involved are
    /// zero-sized.
    ///
    /// In many cases, this will be the same as [`ControlMechanism::ExclusiveAccess`]
    type TemporaryExclusiveAccess<'q, 'containing: 'q>;

    fn grant_short_access<'p: 'q, 'q>(
        broad_access: &'q mut Self::ExclusiveAccess<'p>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'p>;

    /// Shorten a given access even further.
    fn shorten_temporary_access<'orig: 'p, 'p: 'q, 'q>(
        holding_temporary_access: &'q mut Self::TemporaryExclusiveAccess<'p, 'orig>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'orig>;
}

/// A control mechanism that can use any pin which is configured as a GPIO (output, not flex)
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct GpioPinCtrl;

impl ControlMechanism for GpioPinCtrl {
    type ExclusiveAccess<'q> = hal::gpio::Output<'q, hal::gpio::AnyPin>;

    type TemporaryExclusiveAccess<'q, 'containing: 'q> = &'q mut hal::gpio::Output<'containing, hal::gpio::AnyPin>;

    #[inline]
    fn grant_short_access<'p: 'q, 'q>(
        containing: &'q mut Self::ExclusiveAccess<'p>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'p> {
        containing
    }

    #[inline]
    fn shorten_temporary_access<'orig: 'p, 'p: 'q, 'q>(
        holding_temporary_access: &'q mut Self::TemporaryExclusiveAccess<'p, 'orig>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'orig> {
        &mut *holding_temporary_access
    }
}

/// A control mechanism for PIO pins on a specific PIO bank
///
/// Typically, things using this mechanism will likely also require you to provide something
/// like a PIO state machine.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct PioPinCtrl<PIO>(PhantomData<PIO>);

// Static requirement can be removed if we upstream the reborrow thing.
impl<PIO: hal::pio::Instance + 'static> ControlMechanism for PioPinCtrl<PIO> {
    type ExclusiveAccess<'q> = hal::pio::Pin<'q, PIO>;

    type TemporaryExclusiveAccess<'q, 'containing: 'q> = &'q mut hal::pio::Pin<'containing, PIO>;

    // Note - we should make a PR to allow for [`hal::PeripheralRef::reborrow()`] on pio pins.
    // The pin itself contains such a ref anyhow.
    #[inline]
    fn grant_short_access<'p: 'q, 'q>(
        broad_access: &'q mut Self::ExclusiveAccess<'p>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'p> {
        broad_access
    }

    #[inline]
    fn shorten_temporary_access<'orig: 'p, 'p: 'q, 'q>(
        holding_temporary_access: &'q mut Self::TemporaryExclusiveAccess<'p, 'orig>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'orig> {
        &mut *holding_temporary_access
    }
}

/// A control mechanism that does nothing. Most implementations should provide this option, as
/// it can be used to indicate that, for instance, there is no actual need for a given
/// component.
///
/// For example, it might be used in the case of a bus implementation that normally takes a
/// chip-select, but your hardware bus only points to one single chip and - most importantly -
/// there is no chip select system in place (no pin that must be activated). If there was one
/// that needed activation, you'd want to use another control mechanism.
///
/// This is also useful in the case where you need to use a [`super::GlobalModeProvider`] as a
/// [`super::ModularChipModeStore`].
pub type UnneededCtrl = ();

impl ControlMechanism for UnneededCtrl {
    type ExclusiveAccess<'q> = ();

    type TemporaryExclusiveAccess<'q, 'containing: 'q> = ();

    #[inline]
    fn grant_short_access<'p: 'q, 'q>(
        broad_access: &'q mut Self::ExclusiveAccess<'p>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'p> {
    }

    #[inline]
    fn shorten_temporary_access<'orig: 'p, 'p: 'q, 'q>(
        holding_temporary_access: &'q mut Self::TemporaryExclusiveAccess<'p, 'orig>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'orig> {
    }
}

/// A control mechanism that requires one specific piece of hardware. Generally, it's worth
/// avoiding this if you can at all do so.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct SpecificHardwareCtrl<T>(PhantomData<T>);

impl<T: hal::Peripheral<P = T> + 'static> ControlMechanism for SpecificHardwareCtrl<T> {
    type ExclusiveAccess<'q> = hal::PeripheralRef<'q, T>;

    type TemporaryExclusiveAccess<'q, 'containing: 'q> = hal::PeripheralRef<'q, T>;

    #[inline]
    fn grant_short_access<'p: 'q, 'q>(
        broad_access: &'q mut Self::ExclusiveAccess<'p>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'p> {
        broad_access.reborrow()
    }

    #[inline]
    fn shorten_temporary_access<'orig: 'p, 'p: 'q, 'q>(
        holding_temporary_access: &'q mut Self::TemporaryExclusiveAccess<'p, 'orig>,
    ) -> Self::TemporaryExclusiveAccess<'q, 'orig> {
        holding_temporary_access.reborrow()
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
