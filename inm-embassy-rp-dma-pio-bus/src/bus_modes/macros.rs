//! Module containing the macros for [`crate::bus_modes`]
//!
//! These are used to generate otherwise extremely tedious implementations of the various traits.
//!
//! In part, this separation is designed to avoid making rust-analyzer cry.

// allowed because it makes doc comments much easier.
#[allow(unused_imports)]
use super::*;

/// Define a [`ModularChipModeStore`] and a [`ModularChipModeStore::GenericModeIdentifier`] in lockstep.
///
/// Produces a structure generic over `ModeData`, that implements [`ModularChipModeStore`]. It also
/// produces an enum, with each variant associated with a
/// corresponding structure field of type `ModeData`. `ModeData` also defaults to `()`, so the
/// produced type can be used directly in [`device_mode_provider`]
///
/// This actually can produce multiple structures at once using the same set of names. Within a
/// structure, you can use `|` to map multiple mode names to the same storage (so they will share
/// configuration when using that structure).
///
/// Furthermore, if you want to create a structure that maps to existing enum names, you can remove
/// the enum variant definition. Then, this will use the existing enum (which must have been
/// imported into your namespace to get accessed)
#[macro_export]
macro_rules! modular_chip_mode_storage_and_names {
    (
        $(#[$names_meta:meta])*
        $names_vis:vis enum $names_enum:ident $({
            $($(#[$enum_variant_meta:meta])* $name_enum_variant:ident),* $(,)?
        })? $(;)?

        $(
        $(#[$storage_meta:meta])*
        $storage_vis:vis struct $storage_struct:ident $(<$_tt0:tt>)? {
            $($(#[$struct_field_meta:meta])* $($struct_field_enum_variant:ident)|+ => $struct_field_vis:vis $struct_field_name:ident $(: $_tt1:tt)?),*
            $(,)?
        }
        )*
    ) => {
        $crate::modular_chip_mode_storage_and_names!{
            @make_enum_conditionally $({$(
                    $(#[$enum_variant_meta])*
                    $name_enum_variant
            )*})? $(#[$names_meta])* $names_vis enum $names_enum
        }

        $(
            $(#[$storage_meta])*
            $storage_vis struct $storage_struct<ModeData = ()> {$(
                $(#[$struct_field_meta])*
                $struct_field_vis $struct_field_name: ModeData,
            )*}

            impl <ModeData> $crate::bus_modes::ModularChipModeStore for $storage_struct<ModeData> {
                type GenericModeIdentifier = $names_enum;
                type ModeData = ModeData;
                type WithProcessedModeData<NewModeData> = $storage_struct<NewModeData>;

                fn with_processed_mode_data<ModifiedModeData, Error>(
                    self,
                    mut mode_transform: impl FnMut(Self::ModeData) -> Result<ModifiedModeData, Error>,
                ) -> ::core::result::Result<Self::WithProcessedModeData<ModifiedModeData>, Error> {
                    ::core::result::Result::Ok($storage_struct::<ModifiedModeData> {
                        $($struct_field_name: mode_transform(self.$struct_field_name)?),*
                    })
                }

                fn get_mode(&self, identifier: Self::GenericModeIdentifier) -> &Self::ModeData {
                    match identifier {$(
                        $($names_enum::$struct_field_enum_variant)|+ => &self.$struct_field_name,
                    )*}
                }
            }
        )*
    };
    // Exists to avoid the problem of needing to "pull" a ? "around" a repeating macro var.
    (@make_enum_conditionally {
        $($(#[$enum_variant_meta:meta])*
        $name_enum_variant:ident)*
    } $(#[$names_meta:meta])* $names_vis:vis enum $names_enum:ident) => {
        $(#[$names_meta])*
        $names_vis enum $names_enum {$(
            $(#[$enum_variant_meta])*
            $name_enum_variant,
        )*}
    };
    (@make_enum_conditionally $(#[$names_meta:meta])* $names_vis:vis enum $names_enum:ident) => {};
}

pub(super) mod private_examples_modular_chip_mode_storage_and_names {
    crate::modular_chip_mode_storage_and_names! {
        /// Modes for controlling some family of LCDs with a bus.
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
        pub(super) enum LCDBusModes {
            FastRead,
            SlowWrite,
            BulkWrite
        }

        // The generics are optional and don't matter, and the types provided to the fields don't
        // actually matter and are optional. They just are allowed to be there so it can look more
        // like a normal Rust struct definition.
        pub(super) struct LCDBusConfigurations <ModeData> {
            /// Configuration data for the bus to use when switching to fast read mode for this type of
            /// device
            FastRead => pub fast_read: _,
            /// The LCD family needs different bus clocks for slow write and bulk write.
            SlowWrite => pub slow_write: _,
            BulkWrite => pub bulk_write: _
        }

        // For LCDs missing bulk write functionality, you can make a storage where SlowWrite and
        // BulkWrite share a mode configuration.
        pub(super) struct NoBulkWriteLCDBusConfigurations {
            FastRead => pub fast_read: _,
            SlowWrite | BulkWrite => pub write: _
        }
    }

    crate::modular_chip_mode_storage_and_names! {
        /// Modes for controlling a family of rainbow LED strips on the bus
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
        pub(super) enum LEDStripBusModes {
            /// The family of LED strips needs a different bus mode to support uniform colour
            /// control.
            UniformColour,
            /// The family of LED strips needs faster bus timing for full-matrix 3xN colour control
            FullResolution,
        }

        pub(super) struct LEDStripBusConfigurations <ModeData> {
            UniformColour => pub uniform_colour: _,
            FullResolution => pub full_resolution
        }
    }
}

/// Construct [`GlobalModeProvider`] and [`ChipModeProvider`] types (and associated enums) from a
/// list of sets of named devices associated with generic structures implementing
/// [`ModularChipModeStore`].
///
/// These providers are lazy-select providers, that is, [`GlobalModeProvider::with_chip`] does not perform any
/// hardware actions, and relies on the actual driver that is called by
/// [`ChipModeProvider::with_mode`]/[`ChipModeProvider::with_mode_ref`] to do so.
///
/// The provided structs have to implement [`ModularChipModeStore`], and this macro will use the
/// internal [`ModularChipModeStore::WithProcessedModeData`] GAT to generate actual types. This
/// means that you can pass in a structure with many generic parameters, and this macro will always
/// work with it as long as it implements the trait.
///
/// This macro produces 3 different structs:
/// * A [`GlobalModeProvider`], containing fields for each set of chips with distinct modes. These
///   fields have visibilities controlled by the macro caller. 
///
///   This structure will also implement [`ModularChipModeStore`], essentially acting as a hardware
///   mode descriptor "block" that could be used further. While the structure does require an
///   internal control mechanism, the [`hardware_control::ControlMechanism`] trait is implemented
///   for `()`
/// * A [`ChipModeProvider`], which contains extra chip data + control-mechanism, as well as a
///   private enum - each variant (one per chip) holding an exclusive reference to whatever
///   [`ModularChipModeStore`] is held for that chip. The private enum is described below.
/// * A structure that holds all the control mechanisms for each device. This may be useful when
///   creating constructors or with validation of pins.
///
///   Several utility functions are provided on this structre. They are marked as private
///   visibility (but it should be easy to make a `#[inline]` function that calls out to them if
///   you want to make them publically available to users).
///
///   This implements [`super::GlobalSelectionMechanisms`], and is the selection-mechanisms type of
///   the global mode provider structure
///
/// This macro produces 3 different enums:
/// * The enum for the names of each chip that can be selected - This is what's used when picking a chip
///   via [`GlobalModeProvider::with_chip`].
/// * An enum for the names of each chip that can be selected, with a value of the associated
///   identifier for the [`ModularChipModeStore`] used by that chip  (in particular, the identifier
///   value is [`ModularChipModeStore::GenericModeIdentifier`]).
///
///   This acts as [`ChipModeProvider::ModeIdentifier`]. If the selected chip in the chip mode
///   selection is different from the top-level variant of this enum, then it will return [`None`]
///   when trying to select a specific mode for that chip (and naturally the driver function will
///   not be called).
///
///   The enum also acts as the value of [`ModularChipModeStore::GenericModeIdentifier`] in the
///   [`GlobalModeProvider`]'s implementation of [`ModularChipModeStore`], which means that for complex
///   nested configurations we can still obtain a reliable mode identifier.
///
///   Some utility functions are provided on this enum. They are private, but if you want to
///   provide them to your users, a simple `#[inline]` function that calls it will work.
/// * An enum with variants for each chip, with the values being references to the
///   [`ModularChipModeStore`] for the chip.
///
///   This enum is private as it's intended as an internal component of the [`ChipModeProvider`].
///   You can still access it of course, but it's not a good idea to modify it's value inside the
///   [`ChipModeProvider`] in case a client of your API wraps your [`GlobalModeProvider`] to have
///   hardware behaviour on chip selection. Internal APIs to this crate do that, as well. It won't
///   cause memory/program unsafety but it may cause correctness issues.
#[macro_export]
macro_rules! device_mode_provider {
    (
        // Controls the enum with device names. Enum variant names are replicated in the device mode
        // identifier enum name provided. The separate declaration simply allows you to add
        // different meta tags (documentation, derives, etc.)
        //
        // It also allows you to define fields to hold the control mechanism for each device inside the
        // control mechanisms structure.
        $(#[$device_name_enum_meta:meta])*
        $device_name_enum_vis:vis enum $device_name_enum_name:ident {
             $(
                 $(#[$device_name_variant_meta:meta])*
                 $device_name_variant:ident => $device_control_mechanism_vis:vis $device_control_mechanism_field:ident
            ),* $(,)?
        }$(;)?

        // Controls the enum with device control mechanisms. Fields defined in the device name
        // enum.
        //
        // The generics are unnecessary and mostly so it looks more like a generic declaration
        // (which it is).
        $(#[$device_control_struct_meta:meta])*
        $device_control_struct_vis:vis struct $device_control_struct_name:ident $(<$_lt10:lifetime, $_tt10:tt>)?;

        // Enum name for the device + mode identifier enum. Has it's own derives and visibility. The
        // variant names can be the same or different to those in the device name enum (if they are
        // the same though, you can avoid having to repeat it in the macro parts that associate
        // things together).
        //
        // Due to limitations in the macro-by-example system, the names of the enum variants are
        // defined only by referencing them via a `(device_name, device_modes_name)` distinct
        $(#[$device_mode_name_enum_meta:meta])*
        $device_mode_name_enum_vis:vis enum $device_mode_name_enum_name:ident;

        // Name of the enum used inside the ChipModeProvider. This gives no name/attribute control
        // because this is an internal enum. The only control is visibility, and the outermost
        // attibutes (such as doc comments).
        //
        // Has some optional unused generics so it looks a bit more like a declaration
        $(#[$chip_mode_provider_enum_meta:meta])*
        $chip_mode_provider_enum_vis:vis enum $chip_mode_provider_enum_name:ident $(<$_lt0:lifetime, $_tt0:tt>)?;

        // Name, attributes (e.g. documentation), and visibility of the structure defined for the
        // ChipModeProvider.
        //
        // Has unused optional generics so it looks a bit more like a declaration.
        $(#[$chip_mode_provider_struct_meta:meta])*
        $chip_mode_provider_struct_vis:vis struct $chip_mode_provider_struct_name:ident $(<$_lt1:lifetime, $_tt1:tt>)?;

        // Name, attributes (e.g. documentation), visibility, and general implementation and fields
        // for the actual, top-level GlobalModeProvider.
        $(#[$global_mode_provider_meta:meta])*
        $global_mode_provider_vis:vis struct $global_mode_provider_struct_name:ident $(<$_tt2:tt>)? {
            $(
                $(#[$global_mode_provider_field_meta:meta])*
                // This is the core component of how you define which chips are associated with
                // which modular chip mode structures.
                //
                // The chip variant selector and associator looks like one of the following:
                // * A single `name` identifier - in this case, the identifier refers to a variant of the
                //   device name enum and a variant of the device mode name enum, of the same name.
                //   It also associates them together.
                // * A single `(name, modename)` pair - like the single identifier, but allows for
                //   the case that you want to associate a device name with a different modename
                //   This acts to define the enum variant in the device modes enum,
                // * A list of single identifiers in square brackets (like `[cs0, cs1, cs2]`). Like
                //   the single identifier, but with more than one. These chips will share one
                //   common mode storage (though each will still have a unique variant in the
                //   various enums).
                //   This is designed for the case where you have multiple of the same device
                //   (requiring the same mode configuration) on whatever bus system this gets
                //   used in.
                // * A list of `(name, modename)` pairs - like
                //   `[(name0, modename0), (name1, modename1), (name2, modename2)]`. Like the list
                //   of single identifiers, but for the case where the variant names might not
                //   match.
                //
                //   You can mix and match pair-mode definers with shared name modes in the list
                //   selector.
                //
                // The provided type is used to generate the real data storage type using the
                // ModularChipModeStore trait's GAT.
                //
                // The weird bar disambiguates some stuff.
                | $chip_variant_selector_and_associator:tt => $global_mode_provider_field_vis:vis
                    $global_mode_provider_field_name:ident:
                    $global_mode_provider_field_chip_store_generator:ty
            ),* $(,)?
        }$(;)?
    ) => {
        $(#[$device_name_enum_meta])*
        $device_name_enum_vis enum $device_name_enum_name {$(
             $(#[$device_name_variant_meta])*
             $device_name_variant
        ),*}

        $(#[$device_control_struct_meta])*
        $device_control_struct_vis struct $device_control_struct_name <'control, ControlMechanism: $crate::bus_modes::hardware_control::ControlMechanism> {
            // Control mechanism fields for each device
            $(
                $device_control_mechanism_vis $device_control_mechanism_field: <ControlMechanism as $crate::bus_modes::hardware_control::ControlMechanism>::ExclusiveAccess<'control>,
            )*
        }

        impl <'control, ControlMechanism: $crate::bus_modes::hardware_control::ControlMechanism> $device_control_struct_name<'control, ControlMechanism> {
            /// Obtain the chip-selection control mechanism for the given device.
            fn priv_temporarily_access_device_select_control<'s>(
                &'s mut self,
                device_name: &$device_name_enum_name
            ) -> ControlMechanism::TemporaryExclusiveAccess<'s, 'control> where 'control: 's {
                match device_name {
                    $($device_name_enum_name::$device_name_variant => ControlMechanism::grant_short_access(&mut self.$device_control_mechanism_field),)*
                }
            }

            /// Apply a (fallible) function to the control mechanism for each device's selector. This
            /// is good for things like initializing a collection of chip-select pins.
            ///
            /// This occurs precisely in the order in which the device enum was specified.
            fn priv_for_each_device_control<'s, Error>(
                &'s mut self,
                mut selector_control_processor: impl FnMut(
                    ControlMechanism::TemporaryExclusiveAccess<'s, 'control>
                ) -> ::core::result::Result<(), Error>
            ) -> ::core::result::Result<(), Error> where 'control: 's {
                $(
                    let temp_access = ControlMechanism::grant_short_access(&mut self.$device_control_mechanism_field);
                    selector_control_processor(temp_access)?;
                )*
                ::core::result::Result::Ok(())
            }
        }

        // Implement GlobalSelectionMechanisms for the selection mechanism store
        impl <'control, CSMechanism: $crate::bus_modes::hardware_control::ControlMechanism> $crate::bus_modes::GlobalSelectionMechanisms<'control, CSMechanism>
            for $device_control_struct_name<'control, CSMechanism> {
            type ChipIdentifier = $device_name_enum_name;

            #[inline]
            unsafe fn for_each_device_control<'s, Error>(
                &'s mut self,
                modifier: impl FnMut(
                    CSMechanism::TemporaryExclusiveAccess<'s, 'control>,
                ) -> Result<(), Error>,
            ) -> Result<(), Error>
            where
                'control: 's {
                self.priv_for_each_device_control(modifier)
            }


            #[inline]
            unsafe fn apply_to_device<'s, O>(
                &'s mut self,
                device: Self::ChipIdentifier,
                modifier: impl FnOnce(CSMechanism::TemporaryExclusiveAccess<'s, 'control>) -> O,
            ) -> O
            where
                'control: 's {
                let access = self.priv_temporarily_access_device_select_control(&device);
                modifier(access)
            }

        }

        // The GlobalModeProvider structure itself.
        $(#[$global_mode_provider_meta])*
        $global_mode_provider_vis struct $global_mode_provider_struct_name<ModeData = ()> {
            $(
                $(#[$global_mode_provider_field_meta])*
                $global_mode_provider_field_vis $global_mode_provider_field_name: <
                    $global_mode_provider_field_chip_store_generator as $crate::bus_modes::ModularChipModeStore
                >::WithProcessedModeData::<ModeData>,
            )*
        }


        // All the important generation that has to match up things. 
        $crate::device_mode_provider!{@cs_unify
            {$({{$chip_variant_selector_and_associator}; {$global_mode_provider_field_name, $global_mode_provider_field_chip_store_generator}})*};
            { @generate };
            {$device_name_enum_name $device_control_struct_name}
            {$(#[$device_mode_name_enum_meta])* $device_mode_name_enum_vis $device_mode_name_enum_name}
            {$(#[$chip_mode_provider_enum_meta])* $chip_mode_provider_enum_vis $chip_mode_provider_enum_name}
            {$(#[$chip_mode_provider_struct_meta])* $chip_mode_provider_struct_vis $chip_mode_provider_struct_name}
            {$global_mode_provider_struct_name {$($device_name_variant => $device_control_mechanism_field,)*}}
        }
    };
    // This is fully unified. Run the command with the appropriate data structures.
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)* ) => {
        $crate::device_mode_provider!{$($macro_command)* {$({[$(($u_n, $u_mn)),*]; $($u_ad)*})*} $($remaining_args)*}
    };
    // `@cs_unify` macro components are for unifying the form of the chip variant selectors. When
    // they are in unified form, the next provided macro command will be called, with all unified
    // identifiers passed in a big list after macro_command but before remaining_args, along with
    // any extra associated data. macro_command is bundled in { } for disambiguation. So is the
    // selector.
    //
    // The called sub-macro-command looks like:
    //  `macro_command { {[(unified_name, unified_modesname)]; extra data ...} ... } remaining_args`
    //
    // `u_n` = already unified devicename, `u_mn` = already unified modesname `u_ad` = already
    // unified associated data
    // `c_ad` = currently-unifying associated data
    // `non_u_s` = non-unified selectors (for future calls to unify), `non_u_ad` = non-unified
    //   associated data
    //
    // We prefix fully unified chunks with `@` to disambiguate complete chunks. This allows us to
    // do bulk parsing of most common patterns instead of having to do one by one. Though
    // mixed-type selectors require one-by-one parsing
    //
    // Note on the + not * - this stops a matcher from matching all things by leaving both the
    // "existing valid unified bits" and "matcher-unified" bits empty and instead matching only the
    // part that bundles all remaining to-be-unified bits and passing them on (which causes
    // recursion).
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
        {{$single_name_ident:ident}; $($c_ad:tt)* }
        $({{$non_u_s:tt}; $($non_u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)* ) => {
        $crate::device_mode_provider!{@cs_unify {
            // Existing unified data association
            $(@ {{[$(($u_n, $u_mn)),*]}; $($u_ad)*})*
            // Unifying the single name identifiers.
            @ {{[($single_name_ident, $single_name_ident)]}; $($c_ad)*}
            // Unifications not of the same pattern that still need doing
            $({{$non_u_s}; $($non_u_ad)*})*
        }; { $($macro_command)* }; $($remaining_args)*}
    };
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
        $({{($single_name_ident:ident, $single_modesname_ident:ident)}; $($c_ad:tt)* })+
        $({{$non_u_s:tt}; $($non_u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)*) => {
        $crate::device_mode_provider!{@cs_unify {
            // Existing unified data association
            $(@ {{[$(($u_n, $u_mn)),*]}; $($u_ad)*})*
            // Unifying the single pair identifiers.
            $(@ {{[($single_name_ident, $single_modesname_ident)]}; $($c_ad)*})+
            // Unifications not of the same pattern that still need doing
            $({{$non_u_s}; $($non_u_ad)*})*
        }; { $($macro_command)* }; $($remaining_args)*}
    };
    // This unifier takes the common case where there is a list entirely of single names and
    // unifies them all at once. This avoids much recursion.
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
        // bunch of single selectors.
        $({{[$($sub_u_n:ident),* $(,)?]}; $($c_ad:tt)* })+
        $({{$non_u_s:tt}; $($non_u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)* ) => {
        $crate::device_mode_provider!{@cs_unify {
            // Existing unified data association
            $(@ {{[$(($u_n, $u_mn)),*]}; $($u_ad)*})*
            // Mark unified.
            $(@ {{[ $(($sub_u_n, $sub_u_n),)* ]}; $($c_ad)*})+
            // Unifications not of the same pattern that still need doing
            $({{$non_u_s}; $($non_u_ad)*})*
        }; { $($macro_command)* }; $($remaining_args)*}
    };
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
        // In this case, we are unifying mixed lists. So we glob over each list to be unified
        // for identifiers that are pairs (`sub_u_n/mn`, for sub-unified-name/modesname), then over
        // all single-named ones in the current chunk of single-names (`sub_u_c` for currently
        // sub-unifying), then over other arbitrary chunks (`sub_u_nu` for sub-unifying not-unified).
        //
        // We only do one at a time. This avoids any ambiguity.
        //
        // The same need to avoid ambiguity is why we do one-at-a-time of the inner list. We could
        // do a more complicated system like the `@` to reduce recursion, but this is already a
        // pretty massive macro.
        //
        // The most common case (all selectors are single names) is also matched on. This avoids
        // too much recursion.
        {{[
            $(($sub_u_n:ident, $sub_u_mn:ident)),* $(,)?
            $($sub_u_c:ident)+
            $(, $sub_u_nu:tt)* $(,)?
        ]}; $($c_ad:tt)* }
        $({{$non_u_s:tt}; $($non_u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)*) => {
        $crate::device_mode_provider!{@cs_unify {
            // Existing unified data association
            $(@ {{[$(($u_n, $u_mn)),*]}; $($u_ad)*})*
            // Unifying the multi-single identifiers for this chunk
            {{[
                $(($sub_u_n, $sub_u_mn),)*
                $(($sub_u_c, $sub_u_c),)+
                $($sub_u_nu,)*
            ]}; $($c_ad)*}
            // Unifications not of the same pattern that still need doing
            $({{$non_u_s}; $($non_u_ad)*})*
        }; { $($macro_command)* }; $($remaining_args)* }
    };
    // This unifier marks any single completed chunk without an @, with one.
    //
    // This is important when unifying a mixed list.
    (@cs_unify {
        $(@ {{[$(($u_n:ident, $u_mn:ident)),* $(,)?]}; $($u_ad:tt)*})*
        // unified but not marked.
        $({{[$(($sub_u_n:ident, $sub_u_mn:ident)),* $(,)?]}; $($c_ad:tt)* })+
        $({{$non_u_s:tt}; $($non_u_ad:tt)*})*
    }; { $($macro_command:tt)* }; $($remaining_args:tt)* ) => {
        $crate::device_mode_provider!{@cs_unify {
            // Existing unified data association
            $(@ {{[$(($u_n, $u_mn)),*]}; $($u_ad)*})*
            // Mark unified.
            $(@ {{[ $(($sub_u_n, $sub_u_mn),)* ]}; $($c_ad)*})+
            // Unifications not of the same pattern that still need doing
            $({{$non_u_s}; $($non_u_ad)*})*
        }; { $($macro_command)* }; $($remaining_args)*}
    };
    // The unified generation command for all things that need the name -> data bindings in full.
    (@generate {
            $({
                [$(($u_n:ident, $u_mn:ident)),* $(,)?];
                { $shared_modestore_field:ident, $shared_modestore_t:ty }
            })*
        }
        {$device_enum:ident $device_controls_struct:ident}
        {$(#[$modesname_enum_meta:meta])* $modesname_enum_vis:vis $modesname_enum:ident}
        {$(#[$chip_mode_provider_enum_meta:meta])* $chip_mode_provider_enum_vis:vis $chip_mode_provider_enum:ident}
        {$(#[$chip_mode_provider_struct_meta:meta])* $chip_mode_provider_struct_vis:vis $chip_mode_provider_struct:ident}
        // Note - device_variant_name is a duplicate of u_n, but it works better in the situation
        // where we need to pick a control mechanism from a device name.
        {$global_mode_provider_struct:ident {
            $($device_variant_name:ident => $device_control_mechanism_field:ident),* $(,)?
        }}
    ) => {
        // Modesname enum
        $(#[$modesname_enum_meta])*
        $modesname_enum_vis enum $modesname_enum {$($(
            #[doc = concat!(" Modes for [`", stringify!($device_enum), "::", stringify!($u_n), "`]")]
            $u_mn(<$shared_modestore_t as $crate::bus_modes::ModularChipModeStore>::GenericModeIdentifier),
        )*)*}

        impl $modesname_enum {
            const fn priv_device(&self) -> $device_enum {
                match self {
                    $($($modesname_enum::$u_mn(_) => $device_enum::$u_n,)*)*
                }
            }
        }

        // enum containing internal data used inside the chip provider.
        $(#[$chip_mode_provider_enum_meta])*
        $chip_mode_provider_enum_vis enum $chip_mode_provider_enum<'state_ref, ModeData: 'state_ref> {$($(
            #[doc = concat!(" Exclusive access to the mode configuration data for chip [`", stringify!($device_enum), "::", stringify!($u_n), "`]")]
            $u_n(&'state_ref mut <$shared_modestore_t as $crate::bus_modes::ModularChipModeStore>::WithProcessedModeData::<ModeData>),
        )*)*}

        // Chip provider structure
        $(#[$chip_mode_provider_struct_meta])*
        $chip_mode_provider_struct_vis struct $chip_mode_provider_struct<
            'state_ref,
            ModeData: 'state_ref,
            ProvidedChipData,
        > {
            // Probably some kind of ptr - so put at front for alignment or whatever
            raw_mode_access: $chip_mode_provider_enum<'state_ref, ModeData>,
            provided_chip_data: ProvidedChipData
        }

        // What we're here for - chip mode provider!
        impl <
            'state_ref,
            ModeData: 'state_ref,
            ProvidedChipData,
        > $crate::bus_modes::ChipModeProvider::<'state_ref> for $chip_mode_provider_struct<
            'state_ref,
            ModeData,
            ProvidedChipData,
        > {
            type ModeIdentifier = $modesname_enum;
            type ModeData = ModeData;
            type ProvidedChipData = ProvidedChipData;

            fn with_mode<Q>(
                self,
                mode_to_use: &Self::ModeIdentifier,
                controller_ctor: impl FnOnce(
                    &'state_ref Self::ModeData,
                    Self::ProvidedChipData,
                ) -> Q,
            ) -> ::core::option::Option<Q>
            where
                Self::ModeData: 'state_ref {
                use $crate::bus_modes::ModularChipModeStore as MCMS;
                match (self.raw_mode_access, mode_to_use) {$($(
                    ($chip_mode_provider_enum::$u_n(data_ref), $modesname_enum::$u_mn(device_specific_mode)) => ::core::option::Option::Some({
                        (controller_ctor)(
                            <$shared_modestore_t as MCMS>::WithProcessedModeData::<ModeData>::get_mode(data_ref, device_specific_mode),
                            self.provided_chip_data,
                        )
                    }),)*)*
                    // Invalid selected chip + mode pairs
                    _ => return None,
                }
            }


            fn with_mode_ref<'subreference, Q>(
                &'subreference mut self,
                mode_to_use: Self::ModeIdentifier,
                controller_ctor: impl FnOnce(
                    &'state_ref Self::ModeData,
                    &'state_ref mut Self::ProvidedChipData,
                ) -> Q,
            ) -> Option<Q>
            where
                Self::ModeData: 'state_ref,
                Self::ProvidedChipData: 'state_ref,
                'state_ref: 'subreference {

                use $crate::bus_modes::ModularChipModeStore as MCMS;
                match (&mut self.raw_mode_access, mode_to_use) {$($(
                    ($chip_mode_provider_enum::$u_n(data_ref), $modesname_enum::$u_mn(device_specific_mode)) => ::core::option::Option::Some({
                        // The `as` thing is needed to avoid it trying to treat the &mut data_ref
                        // as a ModularChipModeStore via deduction.
                        (controller_ctor)(
                            <$shared_modestore_t as  MCMS>::WithProcessedModeData::<ModeData>::get_mode(&**data_ref, device_specific_mode),
                            &mut self.provided_chip_data,
                        )
                    }),)*)*
                    // Invalid selected chip + mode pairs
                    _ => return None,
                }
            }
        }

        // What we're here for #2 - ModularChipModeStore on the global provider.
        //
        // We do this first because we can reuse some of the stuff in the GlobalModeProvider impl.
        impl <
            ModeData
        > $crate::bus_modes::ModularChipModeStore for $global_mode_provider_struct<ModeData> {
            type GenericModeIdentifier = $modesname_enum;
            type ModeData = ModeData;
            type WithProcessedModeData<NewModeData> = $global_mode_provider_struct<NewModeData>;

            fn with_processed_mode_data<ModifiedModeData, Error>(
                self,
                mut mode_transform: impl FnMut(Self::ModeData) -> Result<ModifiedModeData, Error>
            ) -> ::core::result::Result<Self::WithProcessedModeData<ModifiedModeData>, Error> {
                use ::core::result::Result;
                Result::Ok($global_mode_provider_struct {
                    $($shared_modestore_field: self.$shared_modestore_field.with_processed_mode_data(mode_transform)?,)*
                })
            }

            fn get_mode(&self, identifier: Self::GenericModeIdentifier) -> &Self::ModeData {
                use $modesname_enum as E;
                match identifier {
                    $($(E::$u_mn(inner_mode_descriptor) => self.$shared_modestore_field.get_mode(inner_mode_descriptor),)*)*
                }
            }
        }

        // What we're here for # 3 - The actual GlobalModeProvider impl!
        impl <ModeData> $crate::bus_modes::GlobalModeProvider 
            for $global_mode_provider_struct<ModeData> {
            type ChipIdentifier = $device_enum;
            type ModeData = <Self as $crate::bus_modes::ModularChipModeStore>::ModeData;
            type ChipModes<'s, ProvidedChipData> = $chip_mode_provider_struct<'s, ModeData, ProvidedChipData>
            where
                Self: 's;
            type WithProcessedModeData<NewModeData> = <Self as $crate::bus_modes::ModularChipModeStore>::WithProcessedModeData<NewModeData>;

            fn with_chip<'s, ExtraChipSelectionData>(
                &'s mut self,
                chip_selection: &Self::ChipIdentifier,
                with_provided_extra_data: impl FnOnce() -> ExtraChipSelectionData
            ) -> Self::ChipModes<'s, ExtraChipSelectionData> {
                use $device_enum as DE;
                use $chip_mode_provider_enum as MDE;
                let extra_chip_data = (with_provided_extra_data)();
                let chip_specific_mode_data = match chip_selection {
                    $($(DE::$u_n => MDE::$u_n(&mut self.$shared_modestore_field),)*)*
                };
                $chip_mode_provider_struct {
                    raw_mode_access: chip_specific_mode_data,
                    provided_chip_data: extra_chip_data
                }
            }

            #[inline]
            fn with_processed_mode_data<ModifiedModeData, Error>(
                self,
                mode_transform: impl FnMut(Self::ModeData) -> Result<ModifiedModeData, Error>,
            ) -> Result<Self::WithProcessedModeData<ModifiedModeData>, Error> {
                <Self as $crate::bus_modes::ModularChipModeStore>::with_processed_mode_data(
                    self,
                    mode_transform
                )
            }
        }
    };
}

pub(super) mod private_examples_device_mode_provider {
    use super::private_examples_modular_chip_mode_storage_and_names as private_examples;

    crate::device_mode_provider! {
        #[derive(Clone, Copy, Debug)]
        pub(super) enum BusDevices {
            /// LCD 0, 1, and 3 are the same model, so need the same bus configurations
            LCD0 => lcd_0,
            LCD1 => lcd_1,
            LCD2 => lcd_2,
            LCD3 => lcd_3,
            LEDStrip => led_strip,
        };

        /// Control mechanisms for the selection mechanism of each device.
        pub struct BusChipSelectControls;

        /// All the modes of all the [`BusDevices`]
        #[derive(Clone, Copy, Debug)]
        pub(super) enum BusDeviceModes;

        /// Internal enum which provides &mut for the relevant accessors.
        enum BusDeviceModeDataAccessor;

        /// A selected bus device.
        pub(super) struct SelectedBusDevice;

        /// Actual mode data
        pub(super) struct BusDeviceModeData {
            /// LCD 0 and 1 are the same model, so they can use the same bus configurations
            ///
            /// For whatever reason, we want to have the enum variant for LCD1 which contains all
            /// it's modes, to have a different name to the one used only to refer to the device.
            | [LCD0, (LCD1, LCD1Modes), LCD3] => pub bulk_write_lcds: private_examples::LCDBusConfigurations,
            /// LCD 2 is a special model without a unique bulk write mode. This means it needs a
            /// separate configuration.
            ///
            /// While we could use the generic [`private_examples::LCDBusConfigurations`] structure
            /// and just duplicate mode information between the slow write and bulk write modes
            /// (which are identical for this device), we have a structure that better encodes the
            /// structure in the form of [`private_examples::NoBulkWriteLCDBusConfiguration`],
            /// which has shared bus configuration between slow write and bulk write mode.
            | LCD2 => pub basic_lcds: private_examples::NoBulkWriteLCDBusConfigurations,
            /// We have an LED strip on here too
            | LEDStrip => pub led_strip: private_examples::LEDStripBusConfigurations,
        }
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
