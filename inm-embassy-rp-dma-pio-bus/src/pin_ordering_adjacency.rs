//! Module that allows you (and this module) to validate the ordering and position of pins (in
//! particular, PIO pins)
use embassy_rp as hal;

use crate::const_utils::{const_cmp, const_minmax_u16};

/// How the direction from one pin to another (or an entire sequence) compares to the
/// hardware ordering direction.
///
/// If we think of a clock of hardware pin IDs, then `cis` is `clockwise` and `trans` is
/// `anticlockwise`.
///
/// Also works conceptualised as an ordering, but is more tolerant of the fact that the
/// hardware pins may loop.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Direction {
    /// The sequence/direction matches hardware ordering.
    Cis,
    /// The sequence/direction is opposite hardware ordering 
    Trans,
}

impl Direction {
    /// Flip the cis/trans-ness of the ordering/direction.
    #[inline]
    pub const fn invert(self) -> Self {
        match self {
            Direction::Cis => Direction::Trans,
            Direction::Trans => Direction::Cis,
        }
    }

    /// Flip the cis/trans-ness of the ordering/direction, only if the predicate is true
    #[inline]
    pub const fn invert_if(self, predicate: bool) -> Self {
        if predicate {
            self.invert()
        } else {
            self
        }
    }

    /// Const version of equality.
    #[inline]
    pub const fn const_eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Direction::Cis, Direction::Cis) | (Direction::Trans, Direction::Trans) => true,
            (Direction::Cis, Direction::Trans) | (Direction::Trans, Direction::Cis) => false,
        }
    }

    /// Const version of inequality
    #[inline]
    pub const fn const_neq(&self, other: &Self) -> bool {
        !self.const_eq(other)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum PinBankClassification {
    /// The pins - in provided order - are all next to each other and in hardware-order. Note
    /// that while in theory this would work with looping, the RP2040 has not got accessible
    /// GPIOs #30 and #31, so it doesn't work.
    ///
    /// Note that in the case of *no* pins being provided (an empty slice), this is what will
    /// result.
    CisAdjacent,
    /// The pins - in provided order - are all next to each other and reversed from
    /// hardware-order. Note that while in theory this would work with looping, the RP2040 has
    /// not got accessible GPIOs #31 and #30, so it doesn't work.
    ///
    /// Note that there are some cases where both this and [`PinBankClassification::CisPartialSplit`]
    /// might apply (e.g. when there are two pins and looping). In such cases, the value returned
    /// is this variant.
    TransAdjacent,
    /// The pins - in provided order - are in hardware order, but there is a gap between some
    /// of them. This does still work with hardware looping because there can be a gap.  
    ///
    /// There are some cases where both this and [`PinBankClassification::TransAdjacent`] might
    /// apply (e.g. when there are two pins and there is looping). In such a case, the
    /// trans-adjacency is prioritised.
    CisPartialSplit,
    /// The pins - in provided order - are reversed from hardware order, but there is a gap
    /// between some of them. This will work appropriately with hardware looping. Note that
    /// there are some cases (like with one or two pins) where both [`PinBankClassification::CisAdjacent`]
    /// and [`PinBankClassification::TransPartialSplit`] might apply. In such cases, the cis
    /// version is returned, because the cis direction is checked first.
    TransPartialSplit,
    /// The pins are in some order that is not cis- or trans- with the hardware order.
    OtherOrder,
}

impl PinBankClassification {
    /// Construct a `cis`-ordered result. The boolean is whether or not the pins were adjacent.
    pub const fn cis(is_adjacent: bool) -> Self {
        if is_adjacent {
            Self::CisAdjacent
        } else {
            Self::CisPartialSplit
        }
    }

    /// Construct a `trans`-ordered result. The boolean is whether or not the pins were
    /// reverse-adjacent
    pub const fn trans(is_reverse_adjacent: bool) -> Self {
        if is_reverse_adjacent {
            Self::TransAdjacent
        } else {
            Self::TransPartialSplit
        }
    }

    pub const fn anydir(direction: Direction, is_adjacent: bool) -> Self {
        match direction {
            Direction::Cis => Self::cis(is_adjacent),
            Direction::Trans => Self::trans(is_adjacent),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum PinBankError {
    /// There was a pin repeated in the array of pins.
    ///
    /// Under normal usage, this is hard to obtain due to the singleton system used by most
    /// rust HALs.
    ///
    /// However, functions that take references to uniform pin types may be in the
    /// situation where this occurs, because you provided two references to the same uniform
    /// pin type (presumably by accident, as usually implementors take pins via the singleton
    /// system and then turn them into concrete pin types after).
    ///
    /// This error also is not reliably produced if the pins are not well-ordered. If they are,
    /// this should be detected (because immediately adjacent duplicates are detected, and
    /// non-immediate duplicates imply other-ordering, and overlaps from a full loop cycle also
    /// imply other-ordering).
    DuplicatePin,

    /// If you somehow get a pin that is out of device range this will be produced. Note that
    /// this really shouldn't be possible if you use you HAL's data structures.
    OutOfBoundPin,
}

/// Classify how a collection of PIO pins is ordered. Overlaps are not tolerated (even in
/// looping), and produce either a duplicate pin error or
/// [`PinBankClassification::OtherOrdering`]
pub fn classify_pio_pin_array<'q, 'p: 'q, PIO: hal::pio::Instance + 'p>(
    pins: impl Iterator<Item = &'q hal::pio::Pin<'p, PIO>>,
) -> Result<PinBankClassification, PinBankError> {
    use crate::hardware_constants as hc;
    let raw_pin_ids = pins.map(hal::pio::Pin::pin).map(Into::into);
    classify_raw_pin_number_array::<
        { hc::GPIO_PIN_COUNT },
        { hc::GPIO_PINS_LOOP },
        { hc::GPIO_PINS_LOOP_CONTIGUOUS },
    >(raw_pin_ids)
}

/// Classify a sequence of raw pin numbers into their ordering and adjacency.
///
/// Const parameters:
/// * `DEVICE_PIN_COUNT` - number of pins on the device (for rp2040 this is 30)
/// * `DOES_LOOP` - if the pins should be considered to be looping (that is, the final pin
///   `DEVICE_PIN_COUNT - 1` is conceptually "before" the first pin (pin 0), and in
///   reverse/trans-mode, the last pin is conceptually "after" the first pin.
/// * `LOOP_CONTIGUOUS` - only relevant if the pins loop. When `false`, this indicates that there
///   is a gap between the last pin and the first pin in the loop (i.e. they are considered
///   ordered but not adjacent). When `true`, the first and last pins are considered directly
///   adjacent.
pub fn classify_raw_pin_number_array<
    const DEVICE_PIN_COUNT: u16,
    const DOES_LOOP: bool,
    const LOOP_CONTIGUOUS: bool,
>(
    mut pins: impl Iterator<Item = u16>,
) -> Result<PinBankClassification, PinBankError> {
    type PinId = u16;

    /// Cis vs Trans looping
    ///
    /// If we aren't doing looping, we know whether the pin sequence must be cis or trans within
    /// two pins (if the second is > or < the first). After that, any alternate comparison
    /// direction indicates other-ordered-ness. The boundary point puts a total ordering on the
    /// pins.
    ///
    /// If we are doing looping, then we can only know after 3 pins. Two pins could be cis- or
    /// trans- ordered, because in one of the cases, the loop boundary is crossed (i.e we might
    /// interpret "decreasing" as "increasing so much you go around the loop") - think of it
    /// like an arrow on the circumference of a circle with the first pin having such an arrow
    /// go in both directions away from it to reach the second pin via two paths. In fact, this
    /// is not a total ordering and cis/trans are better thought of as orientation/direction.
    ///
    /// Whichever path the third pin continues, is the real one that then must be checked
    /// further:
    /// * If the third pin is between the first and second (in terms of hardware
    ///   ordering), then the sequence must go through the looping point and the sequence is
    ///   "opposite" the cis/trans-ness of first -> second.
    ///
    ///   This is true because we don't allow more than 1 cycle (otherwise any sequence would be
    ///   cis-ordered or trans-ordered because you could trivially cycle as far around as you needed
    ///   to go for it to work).
    ///
    ///   This means that the "arrow" going the typical direction from first->second would need to
    ///   extend all the way back through first (which is not allowed).
    /// * If the third pin matches the ordering of the first and second w.r.t hardware
    ///   ordering (i.e. continues the circle without looping "after" the second), it trivially
    ///   confirms that the direction remains the same as the "default" for the pair.
    /// * If the third pin is outside the two pins at all, it actually always confirms that the
    ///   sequence must be the same order as the "default" first->second order. All arrows go
    ///   away from the first one and towards the second one, and this still picks the same
    ///   one.
    ///   If the third pin is beyond the looping point, though, then it does cross the loop
    ///   boundary.
    ///
    /// As such, we construct a state machine.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
    enum StateMachine {
        IndeterminateDirSinglePin(PinId),
        /// A known-cis or known-trans sequence without looping (any divergence means non-cis/trans order).
        DeterminedSeq {
            start_pin: PinId,
            end_pin: PinId,
            direction: Direction,
            is_fully_adjacent_so_far: bool,
        },
        /// A pair that could be cis *or* trans depending on which is selected by the next pin
        /// (determining whether to pick of the arrows from that crosses the loop boundary or not).
        ///
        /// The pair is either cis-ordered or trans-ordered without looping.
        IndeterminateDirLoopingSeq {
            start_pin: PinId,
            second_pin: PinId,
        },
        /// A sequence determined to be cis or trans while looping is enabled.  
        DeterminedLoopingSeq {
            start_pin: PinId,
            end_pin: PinId,
            direction: Direction,
            is_fully_adjacent_so_far: bool,
        },
    }

    /// The direction to go from first_pin to second_pin without crossing any loop boundary
    ///
    /// Pins must be distinct. If not, this will return [`None`]
    #[inline]
    const fn loop_free_path_direction(first_pin: PinId, second_pin: PinId) -> Option<Direction> {
        match const_cmp::cmp_u16(&first_pin, &second_pin) {
            core::cmp::Ordering::Less => Some(Direction::Cis),
            core::cmp::Ordering::Equal => None,
            core::cmp::Ordering::Greater => Some(Direction::Trans),
        }
    }

    /// Whether or not the connection going from first_pin -> second_pin crosses the loop
    /// boundary when going via the path with direction indicated by connection_direction.
    ///
    /// If the pins are equal, then this returns [`None`].
    #[inline]
    const fn connection_crosses_loop_point(
        first_pin: PinId,
        second_pin: PinId,
        connection_direction: Direction,
    ) -> Option<bool> {
        match loop_free_path_direction(first_pin, second_pin) {
            Some(v) => Some(v.const_neq(&connection_direction)),
            None => None,
        }
    }

    /// Whether or not the second pin can be considered immediately/adjacently "next" to first
    /// pin, when travelling in the direction given, with the relevant looping selectors
    ///
    /// `LOOP_CONTIGUOUS` dictates whether the crossing point of the loop (at
    /// `DEVICE_PIN_COUNT-1`/`0`), should be considered contiguous/adjacent when being
    /// traversed. Naturally, this is irrelevant if not looping.
    ///
    /// The pins must be < `DEVICE_PIN_COUNT`. Weirdness can result if they are too big.
    const fn is_adjacent_looping<
        const DEVICE_PIN_COUNT: u16,
        const DOES_LOOP: bool,
        const LOOP_CONTIGUOUS: bool,
    >(
        first_pin: PinId,
        second_pin: PinId,
        in_direction: Direction,
    ) -> bool {
        match (first_pin, second_pin, in_direction) {
            (f, s, Direction::Cis) if (f == DEVICE_PIN_COUNT - 1) && (s == 0) => {
                LOOP_CONTIGUOUS && DOES_LOOP
            }
            (f, s, Direction::Trans) if (f == 0) && (s == DEVICE_PIN_COUNT - 1) => {
                LOOP_CONTIGUOUS && DOES_LOOP
            }
            (f, s, Direction::Cis) => f + 1 == s,
            (f, s, Direction::Trans) => s + 1 == f,
        }
    }

    /// Test if the test pin is a continuation of the given path (start_pin -> end_pin via the
    /// given path direction). This requires it not to go all the way past `start_pin` again
    /// (which will return false). If any pins are identical, this returns [`None`]
    ///
    /// Can turn off loop allowance.
    const fn is_continuation_of_path<const DOES_LOOP: bool>(
        start_pin: PinId,
        end_pin: PinId,
        path_direction: Direction,
        test_pin: PinId,
    ) -> Option<bool> {
        let Some(path_crosses_loop) =
            connection_crosses_loop_point(start_pin, end_pin, path_direction)
        else {
            return None;
        };
        let Some(extension_crosses_loop) =
            connection_crosses_loop_point(end_pin, test_pin, path_direction)
        else {
            return None;
        };
        match (path_crosses_loop, extension_crosses_loop, DOES_LOOP) {
            // No loop crossings, so we can do basic comparison.
            (false, false, _) => match path_direction {
                Direction::Cis => Some(test_pin > end_pin),
                Direction::Trans => Some(test_pin < end_pin),
            },
            // Disallow loops if we do not loop
            (_, _, false) => Some(false),
            // The test pin is in the region "after" the loop crossing in the relevant extension
            // direction. So to be a continuation, it must be "before" the start pin in that
            // region.
            (false, true, true) => match path_direction {
                Direction::Cis => Some(test_pin < start_pin),
                Direction::Trans => Some(test_pin > start_pin),
            },
            // The path's endpoint is after the loop in it's direction, and the start point is
            // pre-loop-boundary. That means the test pin must be between the path's end point and start point (the
            // start point will always be further along in the "previous" cycle than the end
            // point is in the "current" cycle because the path crosses the loop point).
            (true, false, true) => match path_direction {
                Direction::Cis => Some(start_pin < test_pin && test_pin < end_pin),
                Direction::Trans => Some(end_pin < test_pin && test_pin < start_pin),
            },
            // Both the path from the startpoint to the endpoint crosses the loop boundary,
            // *and* the path from the endpoint to the testpoint crosses the loop boundary.
            //
            // This can only happen if:
            // * endpoint -> testpoint is the opposite direction to startpoint -> endpoint (so
            // not a continuation)
            // * startpoint -> endpoint and endpoint -> testpoint happen. There is a cross
            //   point in each of these, and because startpoint is before the loop boundary
            //   and testpoint is after it, testpoint must have overlapped startpoint
            //   (basically, startpoint -> testpoint must also have a loop boundary, and as
            //   such there must be at least two complete cycles for both path and extension to
            //   have a crossover point and in the same direction).
            (true, true, true) => Some(false),
        }
    }

    let Some(initial_pin): Option<PinId> = pins.next() else {
        return Ok(PinBankClassification::CisAdjacent);
    };
    if initial_pin >= DEVICE_PIN_COUNT {
        return Err(PinBankError::OutOfBoundPin);
    };
    let mut seq_state = StateMachine::IndeterminateDirSinglePin(initial_pin);
    /// It is extremely common for many functions to return Option<T>, which is [`None`] if the
    /// pins are duplicate. And it is very common for us to do a let ... else { return
    /// Err(PinBankError::DuplicatePin) } on that. However, this takes up 3 lines (and we can't
    /// do ok_or()? because the question mark doesn't work in const fns). So instead we do a
    /// shorthand macro.
    macro_rules! dup {
        ($new_v:ident, $operation:expr) => {
            let Some($new_v) = $operation else {
                return Err(PinBankError::DuplicatePin);
            };
        };
        (mut $new_v:ident, $operation:expr) => {
            let Some(mut $new_v) = $operation else {
                return Err(PinBankError::DuplicatePin);
            };
        };
    }

    /// This performs an `if !is_continuation { return Ok(PinBankClassification::OtherOrder) }`
    /// when given `is_continuation`
    macro_rules! cont {
        ($is_continuation:expr) => {
            if !$is_continuation {
                return Ok(PinBankClassification::OtherOrder);
            };
        };
    }

    for next_pin in pins {
        if next_pin >= DEVICE_PIN_COUNT {
            return Err(PinBankError::OutOfBoundPin);
        };
        seq_state = match seq_state {
            StateMachine::IndeterminateDirSinglePin(start_pin) => {
                if DOES_LOOP {
                    StateMachine::IndeterminateDirLoopingSeq {
                        start_pin,
                        second_pin: next_pin,
                    }
                } else {
                    dup!(direction, loop_free_path_direction(initial_pin, next_pin));
                    StateMachine::DeterminedSeq {
                        start_pin: initial_pin,
                        end_pin: next_pin,
                        direction,
                        is_fully_adjacent_so_far: is_adjacent_looping::<
                            DEVICE_PIN_COUNT,
                            DOES_LOOP,
                            LOOP_CONTIGUOUS,
                        >(
                            initial_pin, next_pin, direction
                        ),
                    }
                }
            }
            StateMachine::DeterminedSeq {
                start_pin,
                end_pin,
                direction,
                is_fully_adjacent_so_far,
            } => {
                dup!(
                    is_continuation,
                    is_continuation_of_path::<DOES_LOOP>(start_pin, end_pin, direction, next_pin,)
                );
                cont!(is_continuation);
                let continuation_adjacency =
                    is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                        end_pin, next_pin, direction,
                    );
                StateMachine::DeterminedSeq {
                    start_pin,
                    end_pin: next_pin,
                    direction,
                    is_fully_adjacent_so_far: is_fully_adjacent_so_far && continuation_adjacency,
                }
            }
            StateMachine::IndeterminateDirLoopingSeq {
                start_pin,
                second_pin,
            } => {
                // We need to determine the direction here.
                dup!(
                    loop_free_direction,
                    loop_free_path_direction(start_pin, second_pin)
                );
                if start_pin == next_pin || second_pin == next_pin {
                    return Err(PinBankError::DuplicatePin);
                };
                let [lowest_pin, highest_pin] = const_minmax_u16([start_pin, second_pin]);
                let needs_inverted_loop_free_direction =
                    (lowest_pin < next_pin) && (next_pin < highest_pin);
                let sequence_direction =
                    loop_free_direction.invert_if(needs_inverted_loop_free_direction);
                let first_pair_adjacent =
                    is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                        start_pin,
                        second_pin,
                        sequence_direction,
                    );
                let continuation_adjacent =
                    is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                        second_pin,
                        next_pin,
                        sequence_direction,
                    );
                // We shouldn't need this because we're always looping in this match branch, but should check
                // anyway.
                dup!(
                    is_continuation,
                    is_continuation_of_path::<DOES_LOOP>(
                        start_pin,
                        second_pin,
                        sequence_direction,
                        next_pin,
                    )
                );
                cont!(is_continuation);
                StateMachine::DeterminedLoopingSeq {
                    start_pin,
                    end_pin: next_pin,
                    direction: sequence_direction,
                    is_fully_adjacent_so_far: first_pair_adjacent && continuation_adjacent,
                }
            }
            StateMachine::DeterminedLoopingSeq {
                start_pin,
                end_pin,
                direction,
                is_fully_adjacent_so_far,
            } => {
                dup!(
                    is_continuation,
                    is_continuation_of_path::<DOES_LOOP>(start_pin, end_pin, direction, next_pin,)
                );
                cont!(is_continuation);
                let continuation_adjacency =
                    is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                        start_pin, end_pin, direction,
                    );
                StateMachine::DeterminedLoopingSeq {
                    start_pin,
                    end_pin: next_pin,
                    direction,
                    is_fully_adjacent_so_far: is_fully_adjacent_so_far && continuation_adjacency,
                }
            }
        };
    }

    match seq_state {
        // sequence is cis
        StateMachine::IndeterminateDirSinglePin(_) => Ok(PinBankClassification::cis(true)),
        StateMachine::DeterminedSeq {
            start_pin: _,
            end_pin: _,
            direction,
            is_fully_adjacent_so_far,
        } => Ok(PinBankClassification::anydir(
            direction,
            is_fully_adjacent_so_far,
        )),
        StateMachine::IndeterminateDirLoopingSeq {
            start_pin,
            second_pin,
        } => {
            if is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                start_pin,
                second_pin,
                Direction::Cis,
            ) {
                Ok(PinBankClassification::CisAdjacent)
            } else if is_adjacent_looping::<DEVICE_PIN_COUNT, DOES_LOOP, LOOP_CONTIGUOUS>(
                start_pin,
                second_pin,
                Direction::Trans,
            ) {
                Ok(PinBankClassification::TransAdjacent)
            } else {
                dup!(
                    cis_needs_loop,
                    connection_crosses_loop_point(start_pin, second_pin, Direction::Cis)
                );
                dup!(
                    trans_needs_loop,
                    connection_crosses_loop_point(start_pin, second_pin, Direction::Trans)
                );
                match (cis_needs_loop, trans_needs_loop, DOES_LOOP) {
                    (false, _, _) => Ok(PinBankClassification::CisPartialSplit),
                    (true, _, true) => Ok(PinBankClassification::CisPartialSplit),
                    (true, false, false) => Ok(PinBankClassification::TransPartialSplit),

                    // this should be unreachable since cis and trans neeed loop should be
                    // essentially opposites. As such we say there is no coherent order.
                    (true, true, _) => Ok(PinBankClassification::OtherOrder),
                }
            }
        }
        StateMachine::DeterminedLoopingSeq {
            start_pin: _,
            end_pin: _,
            direction,
            is_fully_adjacent_so_far,
        } => Ok(PinBankClassification::anydir(
            direction,
            is_fully_adjacent_so_far,
        )),
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
