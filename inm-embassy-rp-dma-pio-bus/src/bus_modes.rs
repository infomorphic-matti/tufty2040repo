//! Module that allows users to define various modes for their buses.
//!
//! This is designed because many buses (serial and parallel) can be switched between modes in a
//! structured way. For instance, they may have:
//! * Multiple *chip-select* pins, which control which device the bus points to.
//! * Each device may have several different modes by which the bus functions (in particular,
//!   timing stuff).
//!
//! This is primarily designed for switching timings, but may allow other behaviour in future.
//!
//! More generally, buses will require mode descriptors that can "compile" into some particular
//! internal representation. You provide information on specific modes (probably using some of the
//! structures in this module), and names for them (which can be things like a pair of indices, or
//! some other things). It will turn them into it's internal compiled representation.
//!
//! And when you want to work with the bus in a certain mode, then you provide the mode name and
//! the handle returned will initialise the bus to use that mode.

pub mod hardware_control;
pub mod macros;
pub mod selection_control;

use core::convert::Infallible;

use embassy_rp as hal;

use crate::parallel::chip_select::ChipSelectBehaviour;

use self::selection_control::{AllUnselected, SelectedChipHandle};

/// Trait for internal representations of mode data.
pub trait CompiledModeData<UserSpecifiedModeData>: Sized {
    /// Possible errors when compiling into internal representation.
    type Error;

    /// Any information structure needed at runtime to go from the user representation to internal
    /// representation.
    type RuntimeInformation;

    /// Turn the user specified mode data into the compiled mode data using the required runtime
    /// data.  
    fn compiled_from(
        user_specified: UserSpecifiedModeData,
        runtime_information: Self::RuntimeInformation,
    ) -> Result<Self, Self::Error>;
}

impl<'q, UserData: 'q + Clone, T: CompiledModeData<UserData>> CompiledModeData<&'q UserData> for T {
    type Error = <T as CompiledModeData<UserData>>::Error;

    type RuntimeInformation = <T as CompiledModeData<UserData>>::RuntimeInformation;

    #[inline]
    fn compiled_from(
        user_specified: &'q UserData,
        runtime_information: Self::RuntimeInformation,
    ) -> Result<Self, Self::Error> {
        <T as CompiledModeData<UserData>>::compiled_from(
            user_specified.clone(),
            runtime_information,
        )
    }
}

impl<'q, UserData: 'q + Clone, T: CompiledModeData<UserData>> CompiledModeData<&'q mut UserData>
    for T
{
    type Error = <T as CompiledModeData<UserData>>::Error;

    type RuntimeInformation = <T as CompiledModeData<UserData>>::RuntimeInformation;

    #[inline]
    fn compiled_from(
        user_specified: &'q mut UserData,
        runtime_information: Self::RuntimeInformation,
    ) -> Result<Self, Self::Error> {
        <T as CompiledModeData<UserData>>::compiled_from(
            user_specified.clone(),
            runtime_information,
        )
    }
}

/// Trait for specific means of storing the control mechanisms to select some chips via the
/// specific control mechanism as a parameter.
///
/// This is used to provide control mechanisms for a [`GlobalModeProvider`].
pub trait GlobalSelectionMechanisms<
    'control,
    ChipSelectMechanism: hardware_control::ControlMechanism,
>
{
    /// Means of identifying a single chip.
    type ChipIdentifier;

    /// Apply a (fallible) function to the control mechanism for each device's selector. This
    /// is good for things like initializing a collection of chip-select pins.
    ///
    /// Note that the order of application is entirely up to the implementor. If the function is
    /// fallible and fails, the control mechanisms that happen to have completed beforehand, is
    /// entirely dependent on the implementor (that is, the operation is not atomic).
    ///
    /// # SAFETY
    /// Users of this function must ensure that they aren't selecting more than one device at the
    /// same time.
    unsafe fn for_each_device_control<'s, Error>(
        &'s mut self,
        modifier: impl FnMut(
            ChipSelectMechanism::TemporaryExclusiveAccess<'s, 'control>,
        ) -> Result<(), Error>,
    ) -> Result<(), Error>
    where
        'control: 's;

    /// Apply a function to the control mechanism for a specific device, returning some arbitary
    /// type
    ///
    /// # SAFETY
    /// Users must ensure they don't select more than one device on the same collection of
    /// selection mechanisms at the same time.
    unsafe fn apply_to_device<'s, O>(
        &'s mut self,
        device: &Self::ChipIdentifier,
        modifier: impl FnOnce(ChipSelectMechanism::TemporaryExclusiveAccess<'s, 'control>) -> O,
    ) -> O
    where
        'control: 's;
}

/// Implement the [`GlobalSelectionMechanisms`] trait on mutable references to implementors of that
/// trait
impl<
        'mref,
        'control: 'mref,
        CSMechanism: hardware_control::ControlMechanism,
        T: GlobalSelectionMechanisms<'control, CSMechanism>,
    > GlobalSelectionMechanisms<'control, CSMechanism> for &'mref mut T
{
    type ChipIdentifier = T::ChipIdentifier;

    #[inline]
    unsafe fn for_each_device_control<'s, Error>(
        &'s mut self,
        modifier: impl FnMut(
            <CSMechanism as hardware_control::ControlMechanism>::TemporaryExclusiveAccess<
                's,
                'control,
            >,
        ) -> Result<(), Error>,
    ) -> Result<(), Error>
    where
        'control: 's,
    {
        T::for_each_device_control(self, modifier)
    }

    #[inline]
    unsafe fn apply_to_device<'s, O>(
        &'s mut self,
        device: &Self::ChipIdentifier,
        modifier: impl FnOnce(
            <CSMechanism as hardware_control::ControlMechanism>::TemporaryExclusiveAccess<
                's,
                'control,
            >,
        ) -> O,
    ) -> O
    where
        'control: 's,
    {
        T::apply_to_device(self, &device, modifier)
    }
}

/// Top level information storage for all the modes supported by a specific hardware bus, for each
/// chip to select (typically using a chip-select pin, but buses often also implement for the case
/// where no pin for selection exists e.g. a bus with only a single connected device and NO pin
/// that needs to be controlled to pick it).
///
/// Define this - together with a [`GlobalSelectionMechanisms`] to access hardware - using the
/// [`crate::device_mode_provider`].
///
/// People wanting to write structures to embed mode data for classes of hardware independent of
/// the specific layout of a bus should look at [`ModularChipModeStore`]. This can be modularly
/// composed inside a [`GlobalModeProvider`].
pub trait GlobalModeProvider {
    /// Means of identifying a single chip.
    type ChipIdentifier;

    /// Mode data
    type ModeData;

    /// Modes for a specific chip, with arbitrary extra data attached to that chip.
    type ChipModes<'s, ProvidedChipData>: ChipModeProvider<
        's,
        ModeData = Self::ModeData,
        ProvidedChipData = ProvidedChipData,
    >
    where
        Self: 's;

    /// Version of this type where the mode data has been transformed.
    ///
    /// Also has the same requirement for the control mechanism to last the same time.
    type WithProcessedModeData<CompiledModeData>: GlobalModeProvider<
        ModeData = CompiledModeData,
        ChipIdentifier = Self::ChipIdentifier,
    >;

    /// Produce a collection of modes that work for the specific chip identified, along with
    /// storing extra data provided by the caller into the produced collection of modes.
    fn with_chip<'s, ExtraChipSelectionData>(
        &'s mut self,
        chip_selection: &Self::ChipIdentifier,
        with_provided_extra_data: impl FnOnce() -> ExtraChipSelectionData,
    ) -> Self::ChipModes<'s, ExtraChipSelectionData>;

    /// Modify all internal mode data with a given fallible processing function.
    ///
    /// This is used for the compiling mechanism.
    fn with_processed_mode_data<ModifiedModeData, Error>(
        self,
        mode_transform: impl FnMut(Self::ModeData) -> Result<ModifiedModeData, Error>,
    ) -> Result<Self::WithProcessedModeData<ModifiedModeData>, Error>;
}

/// Modes for a specific chip. The implementation is fairly flexible.
pub trait ChipModeProvider<'state_reference> {
    /// Means of identifying the mode to select.
    type ModeIdentifier;

    /// Type of individual mode data for the modes available for this chip.
    type ModeData;

    /// Extra chip data that was associated when selecting this chip mode.
    type ProvidedChipData;

    /// Select a specific mode and pass it to something that can use the control mechanism in a way
    /// that consumes the chip selector.
    ///
    /// If you continue to have access to the parent [`GlobalModeProvider`], this function is
    /// almost always the right choice, as you can always drop the resulting structure (which
    /// presumably holds on to the mode information and also a phantom reference to avoid the
    /// chip selection process being called again by someone else and breaking whatever you are
    /// using the result for) and regenerate the chip mode provider from that.  
    ///
    /// Returning [`None`] should only occur if the identifier passed is not a valid mode
    /// identifier.
    ///
    /// Note that in future we might provide an exclusive access/ref to the mode data. That's because
    /// in future it may be possible to use control mechanisms inside mode data. Or something that
    /// wraps control mechanisms for specific bus implementors.
    fn with_mode<Q>(
        self,
        mode_to_use: Self::ModeIdentifier,
        controller_ctor: impl FnOnce(&'state_reference Self::ModeData, Self::ProvidedChipData) -> Q,
    ) -> Option<Q>
    where
        Self::ModeData: 'state_reference;

    /// Select a specific mode for the chip and provide it to something that can handle that mode
    /// data.
    ///
    /// Returning [`None`] should indicate an invalid [`ChipModeProvider::ModeIdentifier`]. Unlike
    /// [`ChipModeProvider::with_mode`], this doesn't consume the chip provider.
    ///
    /// Note that in future we might provide an exclusive access/ref to the mode data. That's
    /// because in future it may be possible to use control mechanisms inside mode data. Or
    /// something that wraps control mechanisms for specific bus implementors.
    fn with_mode_ref<'subreference, Q>(
        &'subreference mut self,
        mode_to_use: Self::ModeIdentifier,
        controller_ctor: impl FnOnce(
            &'state_reference Self::ModeData,
            &'state_reference mut Self::ProvidedChipData,
        ) -> Q,
    ) -> Option<Q>
    where
        Self::ModeData: 'state_reference,
        Self::ProvidedChipData: 'state_reference,
        'state_reference: 'subreference;
}

/// Reusable component for the creation of [`GlobalModeProvider`] structures.
///
/// These are very easy to create using the [`crate::modular_chip_mode_storage_and_names`] macro,
/// which lets you create an enum and struct simultaneously, with enum variants associated with
/// structure fields. It implements this trait on the produced generic structure.
///
/// This is designed so that implementors can create standardised collections of mode names for specific types of
/// hardware (chips), and then someone can construct a [`GlobalModeProvider`] using [`device_mode_provider`]
/// embed such a definition inside a structure encompassing all devices on a bus.
pub trait ModularChipModeStore: Sized {
    /// A generic mode identification type for this storage. Implementors of a [`GlobalModeProvider`] may use this
    /// directly in their own identifiers, or if they need very distinct mode data for each device,
    /// they will probably implement a translation (to reduce the risk of damaging hardware with
    /// similar mode types but very different specifics like perhaps timings or voltages, whatever
    /// their mode data happens to be).
    type GenericModeIdentifier;

    /// Mode data type.
    type ModeData;

    /// Version of this generic chip mode storage device with different mode data.
    type WithProcessedModeData<NewModeData>: ModularChipModeStore<
        GenericModeIdentifier = Self::GenericModeIdentifier,
        ModeData = NewModeData,
    >;

    /// Modify all internal mode data with a given fallible processing function.
    ///
    /// This is used for the compiling mechanism.
    fn with_processed_mode_data<ModifiedModeData, Error>(
        self,
        mode_transform: impl FnMut(Self::ModeData) -> Result<ModifiedModeData, Error>,
    ) -> Result<Self::WithProcessedModeData<ModifiedModeData>, Error>;

    /// Get the mode data for the mode in question from this storage.
    fn get_mode(&self, identifier: Self::GenericModeIdentifier) -> &Self::ModeData;
}

/// Extension trait for [`GlobalModeProvider`]
pub trait GlobalModeProviderExt: GlobalModeProvider {}

/// Contains the controls for hardware devices, as well as the modes that they can take (and which
/// can be selected).
///
/// This is the final stage of managing modes and chip selections. For instance, it will ensure
/// that only one chip is selected at a time.
pub struct GlobalAccessManager<CSControls, GlobalDeviceModes, CSBehaviour> {
    global_mode_provider: GlobalDeviceModes,
    chip_select_controls: AllUnselected<CSControls, CSBehaviour>,
}

impl<
        'controls,
        CSMechanism: hardware_control::ControlMechanism,
        CSControls: GlobalSelectionMechanisms<'controls, CSMechanism>,
        CSBehaviour: ChipSelectBehaviour<CSMechanism>,
        Modes: GlobalModeProvider<ChipIdentifier = CSControls::ChipIdentifier>,
    > GlobalAccessManager<CSControls, Modes, CSBehaviour>
{
    /// Create a new collection of chip-selectors, mode data, and behaviour.
    ///
    /// # SAFETY
    /// Ensure that the chip selection behaviour is actually the right behaviour for the devices on
    /// whatever bus.
    pub unsafe fn new_direct(
        mode_data: Modes,
        chip_select_controls: CSControls,
        chip_select_behaviour: CSBehaviour,
    ) -> Result<Self, CSBehaviour::Error> {
        let unselected_chip_selections =
            unsafe { AllUnselected::new(chip_select_controls, chip_select_behaviour) }?;
        Ok(Self {
            global_mode_provider: mode_data,
            chip_select_controls: unselected_chip_selections,
        })
    }

    /// Create a new collection of chip-selectors, mode data, and behaviour, using an
    /// already-constructed [`AllUnselected`] wrapper struct.
    pub fn new(
        mode_data: Modes,
        chip_select_controls: AllUnselected<CSControls, CSBehaviour>,
    ) -> Self {
        Self {
            global_mode_provider: mode_data,
            chip_select_controls,
        }
    }

    /// Select a specific chip and obtain a [`ChipModeProvider`] for examination of chip mode data.
    pub fn select_device<'s>(
        &'s mut self,
        device: &CSControls::ChipIdentifier,
    ) -> Result<
        Modes::ChipModes<'s, SelectedChipHandle<'s, 'controls, CSMechanism, CSBehaviour>>,
        CSBehaviour::Error,
    > {
        let selected = self.chip_select_controls.select_device(device)?;
        let selected_modes = self.global_mode_provider.with_chip(device, selected);
        Ok(selected_modes)
    }
}

impl<'controls, CSControls, CSBehaviour, Modes: GlobalModeProvider>
    GlobalAccessManager<CSControls, Modes, CSBehaviour>
{
    /// Perform a fallible transformation on the mode data.
    #[inline]
    pub fn modify_mode_data<NewModeData, Error>(
        self,
        transform: impl FnMut(Modes::ModeData) -> Result<NewModeData, Error>,
    ) -> Result<
        GlobalAccessManager<CSControls, Modes::WithProcessedModeData<NewModeData>, CSBehaviour>,
        Error,
    > {
        Ok(Self {
            global_mode_provider: self
                .global_mode_provider
                .with_processed_mode_data(transform)?,
            chip_select_controls: self.chip_select_controls,
        })
    }

    /// Compile the bus modes using (clone-able) runtime data
    pub fn compile_mode_data<NewModeData: CompiledModeData<Modes::ModeData>>(
        self,
        runtime_data: &NewModeData::RuntimeInformation,
    ) -> Result<Modes::WithProcessedModeData<NewModeData>, NewModeData::Error>
    where
        NewModeData::RuntimeInformation: Clone,
    {
        self.modify_mode_data(|old_mode_data| {
            NewModeData::compiled_from(old_mode_data, runtime_data.clone())
        })
    }

    pub const fn chip_select_behaviour(&self) -> &CSBehaviour {
        self.chip_select_controls.chip_select_behaviour()
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient uses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
