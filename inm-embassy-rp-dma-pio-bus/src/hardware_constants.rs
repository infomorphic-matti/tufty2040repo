/// # of GPIO pins
pub const GPIO_PIN_COUNT: u16 = 30;

/// whether the GPIO pins loop around (but the looping may not be contiguous, e.g. there is a
/// "gap" on RP2040 between GPIO #29 and GPIO #0 because there are "fake" #30 and #31 pins due
/// to the underlying core architecture.
pub const GPIO_PINS_LOOP: bool = true;
/// Whether or not the loop crossing point is considered "hardware-adjacent".
pub const GPIO_PINS_LOOP_CONTIGUOUS: bool = false;

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
