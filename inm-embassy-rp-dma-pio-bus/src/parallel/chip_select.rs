//! Module for chip-select stuff in buses with it.

use core::convert::Infallible;

use crate::bus_modes::{
    hardware_control::{ControlMechanism, GpioPinCtrl},
    GlobalSelectionMechanisms,
};

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
pub enum BusChipSelectBehaviour {
    /// The chip is selected when the pin is low (all others should be set high to not be selected)
    SelectedLow,
    /// The chip is selected when the pin is high (all others should be set low to not be
    /// selected).
    SelectedHigh,
}

impl ChipSelectBehaviour<()> for BusChipSelectBehaviour {
    type Error = Infallible;

    #[inline]
    unsafe fn enable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut <() as ControlMechanism>::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }

    #[inline]
    unsafe fn disable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut <() as ControlMechanism>::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl ChipSelectBehaviour<GpioPinCtrl> for BusChipSelectBehaviour {
    type Error = Infallible;

    unsafe fn enable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut <GpioPinCtrl as ControlMechanism>::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error> {
        match self {
            BusChipSelectBehaviour::SelectedLow => device.set_low(),
            BusChipSelectBehaviour::SelectedHigh => device.set_high(),
        };
        Ok(())
    }

    unsafe fn disable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut <GpioPinCtrl as ControlMechanism>::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error> {
        match self {
            BusChipSelectBehaviour::SelectedLow => device.set_high(),
            BusChipSelectBehaviour::SelectedHigh => device.set_low(),
        }
        Ok(())
    }
}

/// Trait for specifically controlling Chip Select control-mechanisms. In particular, performing
/// certain actions to enable/disable/etc. a device, or set the pins to intial state.  
///
/// Most behaviours should always implement this for
/// [`crate::bus_modes::hardware_control::UnneededCtrl`] (that is, a control mechanism of `()`), as
/// this is the case where no control action is needed.
pub trait ChipSelectBehaviour<CSControl: ControlMechanism> {
    /// Error that could occur in operations. You probably want to set this to [`::core::convert::Infallible`] unless you're doing something weird.
    type Error;

    /// Enable a device via the raw chip-select control mechanism.
    ///
    /// Prefer using [`SelectedChipHandle`]
    ///
    /// # SAFETY
    /// Users must ensure that the behaviour of all devices chip-select controls actually
    /// matches this behaviour.
    ///
    /// They must also ensure that they don't enable a second device when a first one is still
    /// enabled.
    unsafe fn enable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut CSControl::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error>;

    /// Disable a device via the raw chip-select control mechanism.
    ///
    /// Prefer using [`SelectedChipHandle`]
    ///
    /// # SAFETY
    /// Users must ensure that the behaviour of all devices chip-select controls actually
    /// matches this behaviour.
    unsafe fn disable_device_raw<'q, 'control: 'q>(
        &self,
        device: &mut CSControl::TemporaryExclusiveAccess<'q, 'control>,
    ) -> Result<(), Self::Error>;
}

/// Extension trait for chip selection behaviours
pub trait ChipSelectBehaviourExt<CSControl: ControlMechanism>:
    ChipSelectBehaviour<CSControl>
{
    /// Ensure that no devices are selected in the provided selection mechanism.
    ///
    /// # SAFETY
    /// Users must ensure that the behaviour of all devices chip-select controls actually
    /// matches this behaviour.
    unsafe fn disable_all_devices<'control>(
        &self,
        devices: &mut impl GlobalSelectionMechanisms<'control, CSControl>,
    ) -> Result<(), Self::Error> {
        devices.for_each_device_control(|mut d| self.disable_device_raw(&mut d))
    }
}

impl<CSMechanism: ControlMechanism, T: ChipSelectBehaviour<CSMechanism> + ?Sized>
    ChipSelectBehaviourExt<CSMechanism> for T
{
}
// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
