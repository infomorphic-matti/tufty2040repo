//! All parallel half-duplex bus stuff (of any width).
pub mod with_data_command;
use crate::{const_utils, NanosDuration};

use super::chip_select::BusChipSelectBehaviour;

/// Control pin descriptor struct, for controlling bus direction and chip select.
///
/// This could contain actual pins, or various relevant processed data structures and types that
/// correspond to those pins.
#[derive(Debug, Clone, Copy)]
pub struct BusControlPins<ChipSelect, ReadPin, WritePin = ReadPin> {
    /// https://en.wikipedia.org/wiki/Chip_select
    ///
    /// Device will only listen to inputs when this is set in a certain way.
    pub chip_select: ChipSelect,
    /// Pin that is used for read mode control (read is initiated by
    /// host, and indication of read completion is also provided by host.
    ///
    /// When triggered, data lines become in control of the device.
    pub read_control: ReadPin,

    /// Pin that is used for write mode control (write is initiated by
    /// host).
    ///
    /// When triggered, data lines become in control of the host.
    pub write_control: WritePin,
}

/// Uniform type for convenience.
pub type UBusControlPins<PT> = BusControlPins<PT, PT, PT>;

impl<CS, RD, WR> BusControlPins<CS, RD, WR> {
    #[inline]
    pub const fn new(chip_select: CS, read_control: RD, write_control: WR) -> Self {
        Self {
            chip_select,
            read_control,
            write_control,
        }
    }
}

/// When the pins have a uniform type, allow bulk transforms
impl<UT> UBusControlPins<UT> {
    #[inline]
    pub fn transform_pins<Q>(self, mut transform: impl FnMut(UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            chip_select: transform(self.chip_select),
            read_control: transform(self.read_control),
            write_control: transform(self.write_control),
        }
    }

    /// Modify the pins in place or otherwise do something with an exclusive reference to
    /// each pin
    #[inline]
    pub fn chain_pins_mut<Q>(&mut self, mut chain: impl FnMut(&mut UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            chip_select: chain(&mut self.chip_select),
            read_control: chain(&mut self.read_control),
            write_control: chain(&mut self.write_control),
        }
    }

    /// Just take the pins and get something from their refs.
    #[inline]
    pub fn chain_pins<Q>(&self, mut chain: impl FnMut(&UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            chip_select: chain(&self.chip_select),
            read_control: chain(&self.read_control),
            write_control: chain(&self.write_control),
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
pub enum BusReadCtrlBehaviour {
    /// The read-control pin is held high, and the actual read process is controlled by a drop and
    /// rise (pulse), where the drop and rise each control the read process (things like releasing
    /// control over the data lines, telling the other device to write, etc.)
    DefaultHighTriggerPulseLow,
    /// The read-control pin is held low, and the actual read process is controlled by a rise and
    /// drop (pulse), where the rise and drop each control the read process (things like releasing
    /// control over the data lines, telling the other device to write, etc.)
    DefaultLowTriggerPulseHigh,
}

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
pub enum BusWriteCtrlBehaviour {
    /// The write-control pin is held high, and the actual write process is controlled by a drop and
    /// rise (pulse), where the drop and rise each control the write process (things like taking
    /// control over the data lines, telling the other device to read, etc.)
    DefaultHighTriggerPulseLow,
    /// The write-control pin is held low, and the actual write process is controlled by a rise and
    /// drop (pulse), where the rise and drop each control the write process (things like taking
    /// control over the data lines, telling the other device to read, etc.)
    DefaultLowTriggerPulseHigh,
}

/// Bundled information on the types and behaviours of the control lines.
pub type BusControlBehaviour =
    BusControlPins<BusChipSelectBehaviour, BusReadCtrlBehaviour, BusWriteCtrlBehaviour>;

/// Configuration for the timings required for the pins on a bidirectional half-duplex parallel bus.
///
/// A lot of the concept of how to structure this comes from the ST7789v Parallel Interface
/// Characteristics section in the specification on page 40.
#[derive(Debug, Clone, Copy)]
pub struct BusTimings {
    /// The minimum pulse length when initiating a read before we can actually read the values off
    /// the bus AND indicate that we've done so. I.e. the bus has to have data on it, and it must
    /// actually be possible for us to indicate that we've read it at the same time.
    pub raw_read_ctrl_min_initiating_pulse_length: NanosDuration,
    /// The minimum possible time between a trigger that we've read the data and when the device
    /// will cease controlling the data bus. If this is low enough, it might be necessary configure read pins in a
    /// separate step (however, our CPU cycles fast enough for this not to be super likely).
    pub read_ctrl_min_data_bus_persist_time: NanosDuration,
    /// The minimum possible time needed after we have read the data bus (and indicated as such
    /// with a pulse) before we can initiate a new read trigger.
    ///
    /// Note that [`BusTimings::read_ctrl_min_data_bus_persist_time`] should be smaller than
    /// this most of the time. If not, then that is the actual minimum time because the data bus
    /// has to be inactive before we can receive or send on it again. This is automatically
    /// detected. A similar situatiuon exists with the
    /// [`with_data_command::BusTimings::data_command_ctrl_min_switch_pre_r_wait_time`].
    pub raw_read_ctrl_min_reset_time: NanosDuration,

    /// The minimum time after initiating a write pulse and setting the data bus pins before we can
    /// instruct the device to read the pins by ending the write pulse (prob. related to
    /// transmission delay and some initialization or something).
    pub raw_write_ctrl_min_initiating_pulse_length: NanosDuration,
    /// This is the minimum time between configuring the data bus with written data and when a
    /// trigger for the device to read that data (end of write pulse) can be produced.
    ///
    /// Most of the time, this is irrelevant (like with
    /// [`BusTimings::raw_write_ctrl_min_initiating_pulse_length`] being bigger than this), but
    /// if this is longer than the minimum initiating pulse length then this is the actual minimum
    /// initiating pulse length.
    pub write_ctrl_min_data_bus_active_time: NanosDuration,
    /// This is the minimum time after we have told the endpoint device to read values from the
    /// data bus that we can re-initiate a write. Note that this is raw_ because there are some
    /// circumstances where this may not be the real value (for instance, if the time that the data
    /// bus must be held in it's signal state after the trigger is too long, or if the data/command
    /// minimum time before switching is longer)
    pub raw_write_ctrl_min_reset_time: NanosDuration,
    /// The minimum time needed for us to hold the data bus's written data after triggering the end
    /// of the write pulse. If this is long enough then it may be necessary to increase the write
    /// reset time in the PIO program.
    pub write_ctrl_min_data_bus_persist_time: NanosDuration,

    /// The minimum time after the chip has been selected that a write can be triggered (in
    /// particular, the end of the pulse)
    ///
    /// If this is less than a certain amount, then we can avoid waiting for chip select before we
    /// do the first operation.
    pub chip_select_min_setup_time_write: NanosDuration,

    /// The minimum time after the chip has been selected that a read can be triggered (in
    /// particular, the end of the pulse)
    ///
    /// If this is less than a certain amount, then we can avoid waiting for chip select before we
    /// start the first operation.
    pub chip_select_min_setup_time_read: NanosDuration,

    /// The minimum time after a write/read that, if we need to mess with the chip select, we can
    /// do so.
    pub chip_select_min_wait_time: NanosDuration,
}

impl BusTimings {
    /// Gets the real minimum read control initiation pulse length. Takes into account other
    /// potential factors.
    pub const fn read_ctrl_min_initiating_pulse_length(&self) -> NanosDuration {
        self.raw_read_ctrl_min_initiating_pulse_length
    }

    /// Gets the real minimum reset time from indication of read to when the next can occur.
    ///
    /// See  [`BusTimings::raw_read_ctrl_min_reset_time`] for info on why this sometimes might
    /// not be that value (basically if the data bus takes too long to reset).
    pub const fn read_ctrl_min_reset_time(&self) -> NanosDuration {
        const_utils::max_duration_u32(
            self.raw_read_ctrl_min_reset_time,
            self.read_ctrl_min_data_bus_persist_time,
        )
    }

    /// Gets the real minimum pulse length from when initiating write to when ending a write pulse
    /// can occur.
    ///
    /// For now, we assume that the initial reconfiguration time is zero (i.e. the written data pins are
    /// updated at the same time as the write trigger). Even if not, we can just extend the write
    /// pulse.
    pub const fn write_ctrl_min_initiating_pulse_length(&self) -> NanosDuration {
        const_utils::max_duration_u32(
            self.raw_write_ctrl_min_initiating_pulse_length,
            self.write_ctrl_min_data_bus_persist_time,
        )
    }

    /// Gets the real minimum reset time from when we tell the device to read from our written
    /// values to when we can start writing more values again.
    ///
    /// Sometimes there are cases where this is longer than expected (in particular, if the persist
    /// time is longer or the data/command is longer).
    pub const fn write_ctrl_min_reset_time(&self) -> NanosDuration {
        const_utils::max_duration_u32(
            self.raw_write_ctrl_min_reset_time,
            self.write_ctrl_min_data_bus_persist_time,
        )
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
