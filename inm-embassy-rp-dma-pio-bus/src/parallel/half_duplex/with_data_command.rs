//! Parallel half-duplex bus stuff of any width, with an extra Data/Command control pin
//! (rather than three pin chip-select/read-control/write-control or other configurations)
use crate::{const_utils, NanosDuration};

use super::{BusChipSelectBehaviour, BusReadCtrlBehaviour, BusWriteCtrlBehaviour};

/// Control pin descriptor struct, for info on pins controlling bus direction and chip select, as
/// well as the addition of the DataOrCommand selection pin
///
/// This could contain actual pins, or various relevant processed data structures and types that
/// correspond to those pins.
#[derive(Debug, Clone, Copy)]
pub struct BusControlPins<ChipSelect, DataOrCommand, ReadPin = DataOrCommand, WritePin = ReadPin> {
    /// "Normal" 3-component cip-select and read-write control pin information
    pub cs_rw_pins: super::BusControlPins<ChipSelect, ReadPin, WritePin>,

    /// Pin that selects whether the transmitted request is relating to data or a command-ish
    /// thing. (in read mode, this usually is provided while writing what data you want to read to
    /// the bus, and often distinguishes between something like wanting bulk display data vs
    /// wanting some single parameter - page 51 in the ST7789 manual is useful for this).
    pub data_or_command_mode: DataOrCommand,
}

/// Uniform-type alias
pub type UBusControlPins<PT> = BusControlPins<PT, PT, PT, PT>;

impl<CS, DCX, RD, WR> BusControlPins<CS, DCX, RD, WR> {
    #[inline]
    pub const fn new(
        chip_select: CS,
        read_control: RD,
        write_control: WR,
        data_or_command_mode: DCX,
    ) -> Self {
        Self {
            cs_rw_pins: super::BusControlPins::new(chip_select, read_control, write_control),
            data_or_command_mode,
        }
    }
}

/// When the pins have a uniform type, allow bulk transforms
impl<UT> UBusControlPins<UT> {
    #[inline]
    pub fn transform_pins<Q>(self, mut transform: impl FnMut(UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            data_or_command_mode: transform(self.data_or_command_mode),
            cs_rw_pins: self.cs_rw_pins.transform_pins(transform),
        }
    }

    /// Modify the pins in place or otherwise do something with an exclusive reference to
    /// each pin
    #[inline]
    pub fn chain_pins_mut<Q>(&mut self, mut chain: impl FnMut(&mut UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            data_or_command_mode: chain(&mut self.data_or_command_mode),
            cs_rw_pins: self.cs_rw_pins.chain_pins_mut(chain),
        }
    }

    /// Just take the pins and get something from their refs.
    #[inline]
    pub fn chain_pins<Q>(&self, mut chain: impl FnMut(&UT) -> Q) -> UBusControlPins<Q> {
        BusControlPins {
            data_or_command_mode: chain(&self.data_or_command_mode),
            cs_rw_pins: self.cs_rw_pins.chain_pins(chain),
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
pub enum BusDataCommandBehaviour {
    /// The data/command pin indicates "data" mode when high, and "command" mode when low.
    DataHighCommandLow,
    /// The data/command pin indicates "data" mode when low, and "command" mode when high.
    DataLowCommandHigh,
}

/// Bundled information on the types and behaviours of the control lines.
///
/// Note that typically, chip-select is controlled by the system created in [`crate::bus_modes`]
/// and related things. To deal with this, the type alias is parameterised on the chip select
/// behaviour, which may be useful.
pub type BusControlBehaviour<CSBehaviour> = BusControlPins<
    CSBehaviour,
    BusDataCommandBehaviour,
    BusReadCtrlBehaviour,
    BusWriteCtrlBehaviour,
>;

/// Configuration for the timings required for the pins on a bidirectional half-duplex parallel bus with data/command pins
///
/// A lot of the concept of how to structure this comes from the ST7789v Parallel Interface
/// Characteristics section in the specification on page 40.
#[derive(Debug, Clone)]
pub struct BusTimings {
    /// Timings of the R/W + CS pins. The processed values for the timings (which are
    /// modified to account for any D/C control timings) are provided by this struct.
    pub rwcs_timings: super::BusTimings,

    /// Minimum time *after* changing the data/command pin's value before any new reads
    /// can be triggered.
    pub data_command_ctrl_min_switch_pre_r_wait_time: NanosDuration,

    /// Minimum time *after* changing the data/command pin's value before any new writes
    /// can be triggered.
    pub data_command_ctrl_min_switch_pre_w_wait_time: NanosDuration,

    /// Minimum time after the end of a read or write control line pulse before we are allowed to
    /// change the data/command pin's value.
    ///
    /// Note that this might not get used at perfect efficiency for now because
    /// we are likely to not change the DC pin until reading the next command. And, for most
    /// purposes, this is less than the r/w wait delay.
    ///
    /// So for now, the only use of this is that if it's bigger than the r/w delay, we
    /// unconditionally increase the r/w delay value (even if the d/c won't change) to ensure no
    /// glitches. However, this switching time is usually very small.
    pub data_command_ctrl_min_switch_post_rw_wait_time: NanosDuration,
}

impl BusTimings {
    /// Gets the real minimum read control initiation pulse length. Takes into account other
    /// potential factors.
    #[inline]
    pub const fn read_ctrl_min_initiating_pulse_length(&self) -> NanosDuration {
        self.rwcs_timings.read_ctrl_min_initiating_pulse_length()
    }

    /// Gets the real minimum reset time from indication of read to when the next can occur.
    ///
    /// See  [`super::BusTimings::raw_read_ctrl_min_reset_time`] for info on why this sometimes might
    /// not be that value (basically if the data bus takes too long to reset).
    #[inline]
    pub const fn read_ctrl_min_reset_time(&self) -> NanosDuration {
        const_utils::max_duration_u32(
            self.rwcs_timings.read_ctrl_min_reset_time(),
            self.data_command_ctrl_min_switch_post_rw_wait_time,
        )
    }

    /// Gets the real minimum pulse length from when initiating write to when ending a write pulse
    /// can occur.
    ///
    /// For now, we assume that the initial reconfiguration time is zero (i.e. the written data pins are
    /// updated at the same time as the write trigger). Even if not, we can just extend the write
    /// pulse (indeed, we can always use the minimum cycle delay on whatever the last
    /// instruction is to initiate the pulse, because we are the bottleneck here not
    /// the device).
    #[inline]
    pub const fn write_ctrl_min_initiating_pulse_length(&self) -> NanosDuration {
        self.rwcs_timings.write_ctrl_min_initiating_pulse_length()
    }

    /// Gets the real minimum reset time from when we tell the device to read from our written
    /// values to when we can start writing more values again.
    ///
    /// Sometimes there are cases where this is longer than expected (in particular, if the persist
    /// time is longer or the data/command is longer).
    #[inline]
    pub const fn write_ctrl_min_reset_time(&self) -> NanosDuration {
        const_utils::max_duration_u32(
            self.rwcs_timings.write_ctrl_min_reset_time(),
            self.data_command_ctrl_min_switch_post_rw_wait_time,
        )
    }

    /// Gets the minimum and maximum timings. May return [`None`] if all timings are filtered out.
    ///
    /// This only includes bundled timings (those constructed by functions in this inherent impl),
    /// and the pre-r/w data/command timings. The post-rw timings are bundled as part of the read
    /// and write control reset times.
    ///
    /// This function allows filtering timings to only include those you care about (e.g. nonzero,
    /// or below a certain amount.
    ///
    /// Output, when Some, is `[min, max]`
    pub fn minmax_timings_including(
        &self,
        allow: impl FnMut(&NanosDuration) -> bool,
    ) -> Option<[NanosDuration; 2]> {
        [
            self.data_command_ctrl_min_switch_pre_r_wait_time,
            self.data_command_ctrl_min_switch_pre_w_wait_time,
            self.read_ctrl_min_reset_time(),
            self.read_ctrl_min_initiating_pulse_length(),
            self.write_ctrl_min_reset_time(),
            self.write_ctrl_min_initiating_pulse_length(),
        ]
        .into_iter()
        .filter(allow)
        .fold(None, |curr_minmax, next_value| match curr_minmax {
            None => Some([next_value, next_value]),
            Some([current_min, current_max]) => Some([
                const_utils::min_duration_u32(current_min, next_value),
                const_utils::max_duration_u32(current_max, next_value),
            ]),
        })
    }
}

/// Generic data for specifying how to control this type of bus - both in terms of high/low, and
/// timings.  
///
/// Generic parameter is for the common case where CS is controlled by the bus mode config rather
/// than the mode, and can be set to `()` 
#[derive(Debug, Clone)]
pub struct BusControlData<CSBehaviour> {
    pub control_behaviours: BusControlBehaviour<CSBehaviour>,
    pub timings: BusTimings 
} 

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
