//! 8-bit specific parallel structure (for buses with 8 bits or 8-bit pin subsections).  

/// 8 bit parallel bus pin descriptor struct.
///
/// May contain actual pins or structured types/data/etc. that correspond to those pins
#[derive(Debug, Clone, Copy)]
pub struct BusDataPins<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> {
    pub pins: (DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7),
}

impl<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> BusDataPins<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> {
    pub const fn new(pins: (DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7)) -> Self {
        Self { pins }
    }
}

/// Data bus with uniform pin type.
pub type UBusDataPins<PT> = BusDataPins<PT, PT, PT, PT, PT, PT, PT, PT>;

/// Uniform pin type transforms
impl<DBP> UBusDataPins<DBP> {
    pub fn transform_pins<Q>(self, mut transform: impl FnMut(DBP) -> Q) -> UBusDataPins<Q> {
        BusDataPins {
            pins: (
                transform(self.pins.0),
                transform(self.pins.1),
                transform(self.pins.2),
                transform(self.pins.3),
                transform(self.pins.4),
                transform(self.pins.5),
                transform(self.pins.6),
                transform(self.pins.7),
            ),
        }
    }

    pub fn chain_pins_mut<Q>(&mut self, mut chain: impl FnMut(&mut DBP) -> Q) -> UBusDataPins<Q> {
        BusDataPins {
            pins: (
                chain(&mut self.pins.0),
                chain(&mut self.pins.1),
                chain(&mut self.pins.2),
                chain(&mut self.pins.3),
                chain(&mut self.pins.4),
                chain(&mut self.pins.5),
                chain(&mut self.pins.6),
                chain(&mut self.pins.7),
            ),
        }
    }

    pub fn chain_pins<Q>(&self, mut chain: impl FnMut(&DBP) -> Q) -> UBusDataPins<Q> {
        BusDataPins {
            pins: (
                chain(&self.pins.0),
                chain(&self.pins.1),
                chain(&self.pins.2),
                chain(&self.pins.3),
                chain(&self.pins.4),
                chain(&self.pins.5),
                chain(&self.pins.6),
                chain(&self.pins.7),
            ),
        }
    }

    pub fn into_array(self) -> [DBP; 8] {
        self.pins.into()
    }
}

// inm-embassy-rp-dma-pio-bus - implements efficient buses for the RP2040
// Copyright (C) 2023  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
