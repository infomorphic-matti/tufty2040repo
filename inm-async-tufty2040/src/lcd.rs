//! Module containing pin-information-traits and similar such things, for the tufty2040
//! lcd peripheral type.
//!
//! In particular, it's the ST7789v, with the following fixed circuitry:
//! * IM0/1/2 pins grounded
//! * LEDK is connected normally and controls the backlight. It should be managed with
//!     pulse width modulation to control brightness.
//! * SDA and GND and MT are grounded.
//! * RESET is connected to 3.3v with a stabilisation capacitor.
//! * Most pins are connected to something with the same name on the chip
//! * TE is connected to !LCD_VSYNC
//! * RS is connected to !LCD_VSYNC
//!
//! Note that the command table set for the ST7789v can be found page 153 -> 158 (after that
//! there is details of every command)
//!
//! There is a second table page 250 -> 254

use core::marker::PhantomData;

use crate::peripherals::LCD0;
use embassy_rp as hal;

mod sealed {
    /// LCD instance
    pub trait Instance {}

    pub trait BacklightPin<T: Instance> {}

    pub trait CsPin<T: Instance> {}
    pub trait RsPin<T: Instance> {}
    pub trait WrPin<T: Instance> {}
    pub trait RdPin<T: Instance> {}

    pub trait DB0Pin<T: Instance> {}
    pub trait DB1Pin<T: Instance> {}
    pub trait DB2Pin<T: Instance> {}
    pub trait DB3Pin<T: Instance> {}
    pub trait DB4Pin<T: Instance> {}
    pub trait DB5Pin<T: Instance> {}
    pub trait DB6Pin<T: Instance> {}
    pub trait DB7Pin<T: Instance> {}
}

pub trait Instance: sealed::Instance {}

impl sealed::Instance for LCD0 {}
impl Instance for LCD0 {}

pub trait BacklightPin<T: Instance>: sealed::BacklightPin<T> + hal::gpio::Pin {}

pub trait CsPin<T: Instance>: sealed::CsPin<T> + hal::gpio::Pin {}
pub trait RsPin<T: Instance>: sealed::RsPin<T> + hal::gpio::Pin {}
pub trait WrPin<T: Instance>: sealed::WrPin<T> + hal::gpio::Pin {}
pub trait RdPin<T: Instance>: sealed::RdPin<T> + hal::gpio::Pin {}

pub trait DB0Pin<T: Instance>: sealed::DB0Pin<T> + hal::gpio::Pin {}
pub trait DB1Pin<T: Instance>: sealed::DB1Pin<T> + hal::gpio::Pin {}
pub trait DB2Pin<T: Instance>: sealed::DB2Pin<T> + hal::gpio::Pin {}
pub trait DB3Pin<T: Instance>: sealed::DB3Pin<T> + hal::gpio::Pin {}
pub trait DB4Pin<T: Instance>: sealed::DB4Pin<T> + hal::gpio::Pin {}
pub trait DB5Pin<T: Instance>: sealed::DB5Pin<T> + hal::gpio::Pin {}
pub trait DB6Pin<T: Instance>: sealed::DB6Pin<T> + hal::gpio::Pin {}
pub trait DB7Pin<T: Instance>: sealed::DB7Pin<T> + hal::gpio::Pin {}

macro_rules! impl_pin {
    ($instance:ident, {$(($function:ident, $pin:ident)),*}) => {
        $(
            impl sealed::$function<$instance> for hal::peripherals::$pin {}
            impl $function<$instance> for hal::peripherals::$pin {}
        )*
    };
}

impl_pin! {
    LCD0, {
        (BacklightPin, PIN_2),

        (CsPin, PIN_10),
        (RsPin, PIN_11),
        (WrPin, PIN_12),
        (RdPin, PIN_13),

        (DB0Pin, PIN_14),
        (DB1Pin, PIN_15),
        (DB2Pin, PIN_16),
        (DB3Pin, PIN_17),
        (DB4Pin, PIN_18),
        (DB5Pin, PIN_19),
        (DB6Pin, PIN_20),
        (DB7Pin, PIN_21)
    }
}

/// A simple structure to make managing DB* pins easier  
pub struct LCDDBBank<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> {
    pub db0: DB0,
    pub db1: DB1,
    pub db2: DB2,
    pub db3: DB3,
    pub db4: DB4,
    pub db5: DB5,
    pub db6: DB6,
    pub db7: DB7,
}

impl<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> LCDDBBank<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> {
    #[inline]
    pub fn new(
        (db0, db1, db2, db3, db4, db5, db6, db7): (DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7),
    ) -> Self {
        Self {
            db0,
            db1,
            db2,
            db3,
            db4,
            db5,
            db6,
            db7,
        }
    }
}

/// Simple utility struct for handling the control bank of LCDs.
pub struct LCDControlBank<CS, RS, WR, RD> {
    pub cs: CS,
    pub rs: RS,
    pub wr: WR,
    pub rd: RD,
}

impl<CS, RS, WR, RD> LCDControlBank<CS, RS, WR, RD> {
    #[inline]
    pub fn new(cs: CS, rs: RS, wr: WR, rd: RD) -> Self {
        Self { cs, rs, wr, rd }
    }
}

/// A utility struct for managing things which are structured like the full LCD io bank
pub struct LCDBank<Backlight, CS, RS, WR, RD, DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> {
    pub backlight: Backlight,
    pub control: LCDControlBank<CS, RS, WR, RD>,
    pub data_bus: LCDDBBank<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7>,
}

impl<Backlight, CS, RS, WR, RD, DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7>
    LCDBank<Backlight, CS, RS, WR, RD, DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7>
{
    #[inline]
    pub fn new(
        backlight: Backlight,
        control: LCDControlBank<CS, RS, WR, RD>,
        data_bus: LCDDBBank<DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7>,
    ) -> Self {
        Self {
            backlight,
            control,
            data_bus,
        }
    }
}

/// Generic trait that provides a plain "structured" collection of types. Can be used to make
/// writing constraints easier and such.
pub trait LCDBankStructure {
    type Backlight;

    type CS;
    type RS;
    type WR;
    type RD;

    type DB0;
    type DB1;
    type DB2;
    type DB3;
    type DB4;
    type DB5;
    type DB6;
    type DB7;
}

impl<Backlight, CS, RS, WR, RD, DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7> LCDBankStructure
    for LCDBank<Backlight, CS, RS, WR, RD, DB0, DB1, DB2, DB3, DB4, DB5, DB6, DB7>
{
    type Backlight = Backlight;

    type CS = CS;
    type RS = RS;
    type WR = WR;
    type RD = RD;

    type DB0 = DB0;
    type DB1 = DB1;
    type DB2 = DB2;
    type DB3 = DB3;
    type DB4 = DB4;
    type DB5 = DB5;
    type DB6 = DB6;
    type DB7 = DB7;
}

/// The internal storage type for peripherals transformed from singletons inside the LCD
/// structure.
type PT<'d, PeripheralType> = hal::PeripheralRef<'d, PeripheralType>;

/// Basic peripheral driver for the LCD.
///
/// Uses PWM to control brightness.
pub struct Lcd<
    'd,
    T: Instance,
    Backlight,
    BacklightPWMSubchannel: crate::pwm::PWMSubchannel,
    PWMChannel: hal::pwm::Channel,
    CS,
    RS,
    WR,
    RD,
    DB0,
    DB1,
    DB2,
    DB3,
    DB4,
    DB5,
    DB6,
    DB7,
> {
    control_bank: LCDControlBank<PT<'d, CS>, PT<'d, RS>, PT<'d, WR>, PT<'d, RD>>,
    db_bank: LCDDBBank<
        PT<'d, DB0>,
        PT<'d, DB1>,
        PT<'d, DB2>,
        PT<'d, DB3>,
        PT<'d, DB4>,
        PT<'d, DB5>,
        PT<'d, DB6>,
        PT<'d, DB7>,
    >,
    pwm_controller: hal::pwm::Pwm<'d, PWMChannel>,
    // Mostly coped from the uart impls
    phantom: PhantomData<(&'d mut T, BacklightPWMSubchannel, &'d mut Backlight)>,
}

impl<
        'd,
        T: Instance,
        Backlight: BacklightPin<T> + crate::pwm::PwmPin<PWMChannelPeripheral, BacklightPWMSubchannel>,
        BacklightPWMSubchannel: crate::pwm::PWMSubchannel,
        PWMChannelPeripheral: hal::pwm::Channel,
        CS: CsPin<T>,
        RS: RsPin<T>,
        WR: WrPin<T>,
        RD: RdPin<T>,
        DB0: DB0Pin<T>,
        DB1: DB1Pin<T>,
        DB2: DB2Pin<T>,
        DB3: DB3Pin<T>,
        DB4: DB4Pin<T>,
        DB5: DB5Pin<T>,
        DB6: DB6Pin<T>,
        DB7: DB7Pin<T>,
    >
    Lcd<
        'd,
        T,
        Backlight,
        BacklightPWMSubchannel,
        PWMChannelPeripheral,
        CS,
        RS,
        WR,
        RD,
        DB0,
        DB1,
        DB2,
        DB3,
        DB4,
        DB5,
        DB6,
        DB7,
    >
{
    /// Construct a new LCD handler.
    ///
    /// Parameters:
    /// * lcd - the lcd peripheral handle.
    /// * pwm_channel - the pulse width modulation channel to use to control the brightness. Each
    ///   pin has one pwm channel it can be modified by.
    /// * pwm_subchannel - while in theory this could be simply provided as a type parameter,
    ///     supplying this here makes type deduction able to function (which is necessary for
    ///     usability given the 16+ type parameters involved. It also dictates whether to use PWM
    ///     channel a or b. Each pin is either channel a or channel b of one PWM peripheral.
    /// * lcd_bank - pins to use for the LCD control and LCD backlighting. The [`LCDBank`]
    ///     structure provides nicer structure to work with when specifying appropriate pins.
    pub fn new(
        lcd: impl hal::Peripheral<P = T> + 'd,
        pwm_channel: impl hal::Peripheral<P = PWMChannelPeripheral> + 'd,
        pwm_subchannel: BacklightPWMSubchannel,
        lcd_bank: LCDBank<
            impl hal::Peripheral<P = Backlight> + 'd,
            impl hal::Peripheral<P = CS> + 'd,
            impl hal::Peripheral<P = RS> + 'd,
            impl hal::Peripheral<P = WR> + 'd,
            impl hal::Peripheral<P = RD> + 'd,
            impl hal::Peripheral<P = DB0> + 'd,
            impl hal::Peripheral<P = DB1> + 'd,
            impl hal::Peripheral<P = DB2> + 'd,
            impl hal::Peripheral<P = DB3> + 'd,
            impl hal::Peripheral<P = DB4> + 'd,
            impl hal::Peripheral<P = DB5> + 'd,
            impl hal::Peripheral<P = DB6> + 'd,
            impl hal::Peripheral<P = DB7> + 'd,
        >,
    ) -> Self {
        let pwm_controller = Backlight::configure_pin_as_pwm_output(
            pwm_channel,
            lcd_bank.backlight,
            crate::pwm::SubchannelAgnosticConfig::default(),
        );

        Self {
            control_bank: todo!(),
            db_bank: todo!(),
            pwm_controller,
            phantom: PhantomData,
        }
    }
}
