//! Async board support crate for the tufty2040 that is designed for the embassy packages.
//!
//! This crate automatically includes the bootloader component because [`embassy_rp`] does
//! too. It selects an appropriate bootloader for the flash hardware on the tufty2040
//!
//! Primarily this provides abstractions over the GPIO pins for use as the specific peripherals on
//! the board, as well as aliases. Note that the embassy HAL provides generic hardware impls
//! already, but they do not all reflect the specific uses of the pin on the tufty2040.
//!
//! As such we provide specific functions to extract the peripherals appropriately for this board.
#![no_std]

pub use embassy_rp as hal;
use hal::{peripherals::UART0, Peripheral};

pub mod lcd;
pub mod macros;
pub mod pwm;

peripherals! {
    LCD0
}

/// Initialize this crate. Call this exactly once.
pub fn init(config: hal::config::Config) -> (hal::Peripherals, Tufty2040Peripherals) {
    // Do this first. Will panic if called twice.
    let hal_peripherals = hal::init(config);
    let tufty2040_peripherals = Tufty2040Peripherals::take();
    (hal_peripherals, tufty2040_peripherals)
}

/// When a composite peripheral exists (such as a UART peripheral), this indicates how to construct
/// it on the Tufty2040 board. Some peripherals implemented in [`embassy_rp`] are not implemented
/// in tufty2040 (for instance, only two pins on the tufty are UART tx/rx, but the `embassy_rp`
/// implements the UART peripheral component traits like TxPin<UART instance> for a lot of
/// different pins).
///
/// This trait is implemented on the instance and is used to construct it properly for the Tufty
/// board. The needed peripherals parameter should be a tuple of generic types that impl
/// [`hal::Peripheral`] so as to enable generic programming.
///
/// It also contains a type parameter for the composite singleton peripheral that is to be globbed
/// in to whatever composite peripheral handler/driver should be constructed.
pub trait Tufty2040CompositePeripheral<CompositePeripheralSingleton: Peripheral, NeededPeripherals>:
    Sized
{
    /// Extra initialization data needed.
    type InitConfig;
    fn construct_from_component_peripherals(
        composite_singleton: CompositePeripheralSingleton,
        p: NeededPeripherals,
        init_config: Self::InitConfig,
    ) -> Self;
}

/// GPIO0 is connected to UART tx
/// GPIO1 is connected to UART rx
///
/// This is the async impl
impl<
        'd,
        S: hal::Peripheral<P = UART0> + 'd,
        Tx: hal::Peripheral<P = hal::peripherals::PIN_0> + 'd,
        Rx: hal::Peripheral<P = hal::peripherals::PIN_1> + 'd,
        TxDMA: hal::Peripheral + 'd,
        RxDMA: hal::Peripheral + 'd,
        IrqBinding: hal::interrupt::typelevel::Binding<
            hal::interrupt::typelevel::UART0_IRQ,
            hal::uart::InterruptHandler<UART0>,
        >,
    > Tufty2040CompositePeripheral<S, (Tx, Rx, TxDMA, RxDMA, IrqBinding)>
    for hal::uart::Uart<'d, UART0, hal::uart::Async>
where
    <TxDMA as hal::Peripheral>::P: hal::dma::Channel,
    <RxDMA as hal::Peripheral>::P: hal::dma::Channel,
{
    type InitConfig = hal::uart::Config;

    #[inline]
    fn construct_from_component_peripherals(
        composite_singleton: S,
        (tx, rx, tx_dma, rx_dma, irq_binding): (Tx, Rx, TxDMA, RxDMA, IrqBinding),
        init_config: Self::InitConfig,
    ) -> Self {
        Self::new(
            composite_singleton,
            tx,
            rx,
            irq_binding,
            tx_dma,
            rx_dma,
            init_config,
        )
    }
}

/// GPIO0 is connected to UART tx
/// GPIO1 is connected to UART rx
///
/// This is sync impl
impl<
        'd,
        S: hal::Peripheral<P = UART0> + 'd,
        Tx: hal::Peripheral<P = hal::peripherals::PIN_0> + 'd,
        Rx: hal::Peripheral<P = hal::peripherals::PIN_1> + 'd,
    > Tufty2040CompositePeripheral<S, (Tx, Rx)>
    for hal::uart::Uart<'d, UART0, hal::uart::Blocking>
{
    type InitConfig = hal::uart::Config;

    #[inline]
    fn construct_from_component_peripherals(
        composite_singleton: S,
        (tx, rx): (Tx, Rx),
        init_config: Self::InitConfig,
    ) -> Self {
        Self::new_blocking(composite_singleton, tx, rx, init_config)
    }
}
