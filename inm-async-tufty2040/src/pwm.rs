//! Utilities for pwm mode control that are pwm channel (A/B) generic.
//!
//! Oftentimes you want to control a given pin using PWM regardless of whether that pin
//! is associated with channel A or B of the PWM controller for the associated pin pairs.
//!
//! This module provides a unifier trait for PWM pins and a/b-agnostic config.

use chain_trans::Trans;
use embassy_rp as hal;

mod sealed {
    /// The given pwm subchannel (a/b), as a typestate.
    pub trait PWMSubchannel {}
}

pub trait PWMSubchannel: sealed::PWMSubchannel {
    /// Specialize a subchannel agnostic config to apply to the relevant subchannel
    ///
    /// Any other subchannels that are not used as part of [`PWMSubchannel`] should be set
    /// to some defaults, the value doesn't matter as typically any other subchannels are unused,
    /// will not be enabled for their respective pins.
    fn specialize_config(subchannel_agnostic_config: SubchannelAgnosticConfig) -> hal::pwm::Config;
}

macro_rules! impl_pwm_subchannel {
    ($($vis:vis $subchannel:ident ($converter_fn:ident);)*) => {
        pub mod subchannel {$(
            #[derive(Debug, Default, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
            $vis struct $subchannel;
        )*}

        $(
        impl sealed::PWMSubchannel for subchannel::$subchannel {}
        impl PWMSubchannel for subchannel::$subchannel {
            #[inline]
            fn specialize_config(subchannel_agnostic_config: SubchannelAgnosticConfig) -> hal::pwm::Config {
                SubchannelAgnosticConfig::$converter_fn(subchannel_agnostic_config)
            }
        }
        )*
    };
}

impl_pwm_subchannel!(
    pub A (specialize_for_subchannel_a);
    pub B (specialize_for_subchannel_b);
);

/// Subchannel-Agnostic verion of [`hal::pwm::Config`]
#[non_exhaustive]
#[derive(Clone)]
pub struct SubchannelAgnosticConfig {
    /// Invert the relevant subchannel
    ///
    /// See [`hal::pwm::Config::invert_a`] and [`hal::pwm::Config::invert_b`]
    pub invert: bool,

    /// Perform phase correction (if true, "flips" the waveform each cycle, except
    /// this results in doubling the length of the cycle)
    ///
    /// See [`hal::pwm::Config::phase_correct`]
    pub phase_correct: bool,

    /// Allow the PWM to produce an output (or not).  
    ///
    /// See [`hal::pwm::Config::enable`]
    pub enable: bool,

    /// A fractional clock divider (increases the length of the
    /// PWM cycle by this amount)
    ///
    /// See [`hal::pwm::Config::divider`]
    pub divider: fixed::FixedU16<fixed::types::extra::U4>,

    /// The comparison point used to determine when to turn the PWM output off. When the PWM
    /// counter is less than this value, it turns on (unless there is inversion). So if this is
    /// zero, then the output on the appropriate subchannel is always low (or high if inverting).
    ///
    /// See [`hal::pwm::Config::compare_a`] and [`hal::pwm::Config::compare_b`]
    pub compare: u16,

    /// The value at which the next counter iteration in the PWM will cycle it back to the start.
    /// (or with phase correct, start moving backwards). The actual number of values taken by the
    /// PWM counter is `top + 1`
    ///
    /// See [`hal::pwm::Config::top`]
    pub top: u16,
}

impl SubchannelAgnosticConfig {
    /// Extract config values from subchannel A of the full PWM channel config provided
    pub fn from_subchannel_a(specific_config: hal::pwm::Config) -> Self {
        Self {
            invert: specific_config.invert_a,
            phase_correct: specific_config.phase_correct,
            enable: specific_config.enable,
            divider: specific_config.divider,
            compare: specific_config.compare_a,
            top: specific_config.top,
        }
    }

    /// Extract config values from subchannel B of the full PWM channel config provided
    pub fn from_subchannel_b(specific_config: hal::pwm::Config) -> Self {
        Self {
            invert: specific_config.invert_b,
            phase_correct: specific_config.phase_correct,
            enable: specific_config.enable,
            divider: specific_config.divider,
            compare: specific_config.compare_b,
            top: specific_config.top,
        }
    }

    /// Specialize the config for subchannel A (filling in B with defaults)
    pub fn specialize_for_subchannel_a(self) -> hal::pwm::Config {
        // Cant do ..Default::default() with non-exhaustive structs.
        hal::pwm::Config::default().trans_mut(|c| {
            c.invert_a = self.invert;
            c.phase_correct = self.phase_correct;
            c.enable = self.enable;
            c.divider = self.divider;
            c.compare_a = self.compare;
            c.top = self.top;
        })
    }

    /// Specialize the config for subchannel B (filling in A with defaults)
    pub fn specialize_for_subchannel_b(self) -> hal::pwm::Config {
        hal::pwm::Config::default().trans_mut(|c| {
            c.invert_b = self.invert;
            c.phase_correct = self.phase_correct;
            c.enable = self.enable;
            c.divider = self.divider;
            c.compare_b = self.compare;
            c.top = self.top;
        })
    }

    /// Specialize the config into both subchannels A and B
    pub fn specialize_for_subchannels_ab(self) -> hal::pwm::Config {
        hal::pwm::Config::default().trans_mut(|c| {
            c.invert_a = self.invert;
            c.invert_b = self.invert;
            c.phase_correct = self.phase_correct;
            c.enable = self.enable;
            c.divider = self.divider;
            c.compare_a = self.compare;
            c.compare_b = self.compare;
            c.top = self.top;
        })
    }
}

impl Default for SubchannelAgnosticConfig {
    fn default() -> Self {
        // Extract defaults
        Self::from_subchannel_a(hal::pwm::Config::default())
    }
}

/// Like [hal::pwm::PwmPinA] and [hal::pwm::PwmPinB], but the mode is a [`BacklightPWMMode`]
pub trait PwmPin<T: hal::pwm::Channel, M: PWMSubchannel>: hal::gpio::Pin {
    /// Configure the pin in question as a PWM output with the given config.
    fn configure_pin_as_pwm_output<'d>(
        channel: impl hal::Peripheral<P = T> + 'd,
        pin: impl hal::Peripheral<P = Self> + 'd,
        config: SubchannelAgnosticConfig,
    ) -> hal::pwm::Pwm<'d, T>;
}

impl<T: hal::pwm::Channel, P: hal::gpio::Pin + hal::pwm::PwmPinA<T>> PwmPin<T, subchannel::A>
    for P
{
    #[inline]
    fn configure_pin_as_pwm_output<'d>(
        channel: impl hal::Peripheral<P = T> + 'd,
        pin: impl hal::Peripheral<P = Self> + 'd,
        config: SubchannelAgnosticConfig,
    ) -> hal::pwm::Pwm<'d, T> {
        hal::pwm::Pwm::new_output_a(channel, pin, subchannel::A::specialize_config(config))
    }
}
impl<T: hal::pwm::Channel, P: hal::gpio::Pin + hal::pwm::PwmPinB<T>> PwmPin<T, subchannel::B>
    for P
{
    #[inline]
    fn configure_pin_as_pwm_output<'d>(
        channel: impl hal::Peripheral<P = T> + 'd,
        pin: impl hal::Peripheral<P = Self> + 'd,
        config: SubchannelAgnosticConfig,
    ) -> hal::pwm::Pwm<'d, T> {
        hal::pwm::Pwm::new_output_b(channel, pin, subchannel::B::specialize_config(config))
    }
}
